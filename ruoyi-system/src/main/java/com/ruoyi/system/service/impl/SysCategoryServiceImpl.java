package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.mapper.SysCategoryMapper;
import com.ruoyi.system.service.ISysCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分类管理 服务实现
 * 
 * @author ruoyi
 */
@Service
public class SysCategoryServiceImpl implements ISysCategoryService
{
    @Autowired
    private SysCategoryMapper deptMapper;

    /**
     * 查询分类管理数据
     * 
     * @param dept 分类信息
     * @return 分类信息集合
     */
    @Override
    @DataScope(tableAlias = "d")
    public List<SysCategory> selectDeptList(SysCategory dept)
    {
        return deptMapper.selectDeptList(dept);
    }

    /**
     * 查询分类管理树
     * 
     * @param dept 分类信息
     * @return 所有分类信息
     */
    @Override
    @DataScope(tableAlias = "d")
    public List<Map<String, Object>> selectDeptTree(SysCategory dept)
    {
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysCategory> deptList = deptMapper.selectDeptList(dept);
        trees = getTrees(deptList, false, null);
        return trees;
    }

    /**
     * 根据角色ID查询分类（数据权限）
     *
     * @param role 角色对象
     * @return 分类列表（数据权限）
     */
    @Override
    public List<Map<String, Object>> roleDeptTreeData(SysRole role)
    {
        Long roleId = role.getRoleId();
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysCategory> deptList = selectDeptList(new SysCategory());
        if (StringUtils.isNotNull(roleId))
        {
            List<String> roleDeptList = deptMapper.selectRoleDeptTree(roleId);
            trees = getTrees(deptList, true, roleDeptList);
        }
        else
        {
            trees = getTrees(deptList, false, null);
        }
        return trees;
    }

    /**
     * 对象转分类树
     *
     * @param deptList 分类列表
     * @param isCheck 是否需要选中
     * @param roleDeptList 角色已存在菜单列表
     * @return
     */
    public List<Map<String, Object>> getTrees(List<SysCategory> deptList, boolean isCheck, List<String> roleDeptList)
    {

        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        for (SysCategory dept : deptList)
        {
            if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()))
            {
                Map<String, Object> deptMap = new HashMap<String, Object>();
                deptMap.put("id", dept.getDeptId());
                deptMap.put("pId", dept.getParentId());
                deptMap.put("name", dept.getDeptName());
                deptMap.put("title", dept.getDeptName());
                if (isCheck)
                {
                    deptMap.put("checked", roleDeptList.contains(dept.getDeptId() + dept.getDeptName()));
                }
                else
                {
                    deptMap.put("checked", false);
                }
                trees.add(deptMap);
            }
        }
        return trees;
    }

    /**
     * 查询分类人数
     * 
     * @param parentId 分类ID
     * @return 结果
     */
    @Override
    public int selectDeptCount(Long parentId)
    {
        SysCategory dept = new SysCategory();
        dept.setParentId(parentId);
        return deptMapper.selectDeptCount(dept);
    }

    /**
     * 查询分类是否存在用户
     * 
     * @param deptId 分类ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(Long deptId)
    {
        int result = deptMapper.checkDeptExistUser(deptId);
        return result > 0 ? true : false;
    }

    /**
     * 删除分类管理信息
     * 
     * @param deptId 分类ID
     * @return 结果
     */
    @Override
    public int deleteDeptById(Long deptId)
    {
        return deptMapper.deleteDeptById(deptId);
    }

    /**
     * 新增保存分类信息
     * 
     * @param dept 分类信息
     * @return 结果
     */
    @Override
    public int insertDept(SysCategory dept)
    {
        SysCategory info = deptMapper.selectDeptById(dept.getParentId());
        // 如果父节点不为"正常"状态,则不允许新增子节点
        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus()))
        {
            throw new BusinessException("分类停用，不允许新增");
        }
        dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
        return deptMapper.insertDept(dept);
    }

    /**
     * 修改保存分类信息
     * 
     * @param dept 分类信息
     * @return 结果
     */
    @Override
    public int updateDept(SysCategory dept)
    {
        SysCategory info = deptMapper.selectDeptById(dept.getParentId());
        if (StringUtils.isNotNull(info))
        {
            String ancestors = info.getAncestors() + "," + info.getDeptId();
            dept.setAncestors(ancestors);
            updateDeptChildren(dept.getDeptId(), ancestors);
        }
        int result = deptMapper.updateDept(dept);
        if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()))
        {
            // 如果该分类是启用状态，则启用该分类的所有上级分类
            updateParentDeptStatus(dept);
        }
        return result;
    }

    /**
     * 修改该分类的父级分类状态
     * 
     * @param dept 当前分类
     */
    private void updateParentDeptStatus(SysCategory dept)
    {
        String updateBy = dept.getUpdateBy();
        dept = deptMapper.selectDeptById(dept.getDeptId());
        dept.setUpdateBy(updateBy);
        deptMapper.updateDeptStatus(dept);
    }

    /**
     * 修改子元素关系
     * 
     * @param deptId 分类ID
     * @param ancestors 元素列表
     */
    public void updateDeptChildren(Long deptId, String ancestors)
    {
        SysCategory dept = new SysCategory();
        dept.setParentId(deptId);
        List<SysCategory> childrens = deptMapper.selectDeptList(dept);
        for (SysCategory children : childrens)
        {
            children.setAncestors(ancestors + "," + dept.getParentId());
        }
        if (childrens.size() > 0)
        {
            deptMapper.updateDeptChildren(childrens);
        }
    }

    /**
     * 根据分类ID查询信息
     * 
     * @param deptId 分类ID
     * @return 分类信息
     */
    @Override
    public SysCategory selectDeptById(Long deptId)
    {
        return deptMapper.selectDeptById(deptId);
    }

    /**
     * 校验分类名称是否唯一
     * 
     * @param dept 分类信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(SysCategory dept)
    {
        Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
        SysCategory info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
        if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue())
        {
            return UserConstants.DEPT_NAME_NOT_UNIQUE;
        }
        return UserConstants.DEPT_NAME_UNIQUE;
    }
}
