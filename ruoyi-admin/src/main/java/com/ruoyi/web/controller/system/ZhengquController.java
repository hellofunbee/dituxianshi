package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.Lvyou;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.domain.Zhengqu;
import com.ruoyi.system.service.ISysCategoryService;
import com.ruoyi.system.service.IZhengquService;
import com.ruoyi.system.util.MyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 区划地名 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Controller
@RequestMapping("/system/zhengqu")
public class ZhengquController extends BaseController
{
	private static final Logger log = LoggerFactory.getLogger(SysProfileController.class);
    private String prefix = "system/zhengqu";

	@Autowired
	private MyWebScoketController websc;
	@Autowired
	private IZhengquService zhengquService;
	@Autowired
	private ISysCategoryService categoryService;
	@RequiresPermissions("system:zhengqu:view")
	@GetMapping()
	public String zhengqu()
	{
	    return prefix + "/zhengqu";
	}
	
	/**
	 * 查询区划地名列表
	 */
	@RequiresPermissions("system:zhengqu:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Zhengqu zhengqu)
	{
		startPage();
		zhengqu.setZqRoleName(ShiroUtils.getSysUser().getUserName());
		zhengquService.updateZhengquRoleName(zhengqu);
		zhengqu.setZqTitle(MyUtil.getSql(zhengqu.getZqTitle(),"zq_title"));
		zhengqu.setZqDetails(MyUtil.getSql(zhengqu.getZqDetails(),"zq_details"));
        List<Zhengqu> list = zhengquService.selectZhengquList(zhengqu);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出区划地名列表
	 */
	@RequiresPermissions("system:zhengqu:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Zhengqu zhengqu)
    {
		zhengqu.setZqTitle(MyUtil.getSql(zhengqu.getZqTitle(),"zq_title"));
		zhengqu.setZqDetails(MyUtil.getSql(zhengqu.getZqDetails(),"zq_details"));

		List<Zhengqu> list = zhengquService.selectZhengquList(zhengqu);
        ExcelUtil<Zhengqu> util = new ExcelUtil<Zhengqu>(Zhengqu.class);
        return util.exportExcel(list, "政区信息");
    }
	
	/**
	 * 新增区划地名
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存区划地名
	 */
	@RequiresPermissions("system:zhengqu:add")
	@Log(title = "区划地名", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Zhengqu zhengqu)
	{
		zhengqu.setCreateBy(ShiroUtils.getLoginName());
		return toAjax(zhengquService.insertZhengqu(zhengqu));
	}

	/**
	 * 修改区划地名
	 */
	@GetMapping("/edit/{zqId}")
	public String edit(@PathVariable("zqId") Integer zqId, ModelMap mmap)
	{
		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(zhengqu.getZqChangedate()!=null) {
			String changedateString = formatter.format(zhengqu.getZqChangedate());
			zhengqu.setZqNewChangedate(changedateString);
		}
		mmap.put("zhengqu", zhengqu);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存区划地名
	 */
	@RequiresPermissions("system:zhengqu:edit")
	@Log(title = "区划地名", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Zhengqu zhengqu)
	{
		zhengqu.setUpdateBy(ShiroUtils.getLoginName());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date currentTime_2  = formatter.parse(zhengqu.getZqNewChangedate(),pos);
		zhengqu.setZqChangedate(currentTime_2);
		return toAjax(zhengquService.updateZhengqu(zhengqu));
	}
	
	/**
	 * 删除区划地名
	 */
	@RequiresPermissions("system:zhengqu:remove")
	@Log(title = "区划地名", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(zhengquService.deleteZhengquByIds(ids));
	}

	/**
	 * 预览区划地名
	 */
	@GetMapping("/info/{zqId}")
	public String info(@PathVariable("zqId") Integer zqId, ModelMap mmap)
	{
		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(zhengqu.getCreateTime());
		if(zhengqu.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(zhengqu.getUpdateTime());
			zhengqu.setNewupdateTime(updatedateString);
		}
		if(zhengqu.getZqChangedate()!=null) {
			String changedateString = formatter.format(zhengqu.getZqChangedate());
			zhengqu.setZqNewChangedate(changedateString);
		}
		zhengqu.setNewcreateTime(createdateString);

		mmap.put("zhengqu", zhengqu);
		return prefix + "/info";
	}

	/**
	 * 预览区划地名
	 */
	@GetMapping("/infoFront/{zqId}")
	public String infoFront(@PathVariable("zqId") Integer zqId, ModelMap mmap, SysCategory category, String title)
	{
		SysCategory categoryNew = new SysCategory();

		if (category == null) {
			category = new SysCategory();
		}

		//所有的节点
		SysCategory c = new SysCategory();
		c.setParentId(100L);
		List<SysCategory> categoryList = categoryService.selectDeptList(c);

		//子目录
		List<SysCategory> childs = new ArrayList<>();

		if (category.getLevel() == null || category.getLevel() == 1) {
			if (category.getDeptId() == null) {
				categoryNew.setParentId(categoryList.get(0).getDeptId());
			} else {
				for (SysCategory n : categoryList) {
					if (n.getDeptId() == category.getDeptId()) {
						n.setCurr(true);
						categoryNew.setParentId(n.getDeptId());
					}
				}
			}
			childs = categoryService.selectDeptList(categoryNew);

		} else if (category.getLevel() == 2) {
			category = categoryService.selectDeptById(category.getDeptId());
			category.setCurr(true);
			categoryNew.setParentId(category.getParentId());
			//选中父级
			for (SysCategory n : categoryList) {
				if (n.getDeptId() == category.getParentId()) {
					n.setCurr(true);
				}
			}
			childs = categoryService.selectDeptList(categoryNew);
			//选中子级
			for (SysCategory c1 : childs) {
				if (c1.getDeptId() == category.getDeptId()) {
					System.out.println(c1.getDeptId() + "=="+ category.getDeptId());
					c1.setCurr(true);
				}
			}
		}
		mmap.put("nodes", categoryList);
		mmap.put("title", title);
		mmap.put("curr", categoryNew);
		mmap.put("curr_child", category);
		mmap.put("childs", childs);

		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(zhengqu.getCreateTime());
		if(zhengqu.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(zhengqu.getUpdateTime());
			zhengqu.setNewupdateTime(updatedateString);
		}
		if(zhengqu.getZqChangedate()!=null) {
			String changedateString = formatter.format(zhengqu.getZqChangedate());
			zhengqu.setZqNewChangedate(changedateString);
		}
		zhengqu.setNewcreateTime(createdateString);

		mmap.put("zhengqu", zhengqu);
		return prefix + "/infoFront";
	}

	/**
	 * 图面化
	 */
	@GetMapping("/avatar/{zqId}")
	public String avatar(@PathVariable("zqId") Integer zqId, ModelMap mmap) {
		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		mmap.put("zhengqu", zhengqu);
		return prefix + "/avatar";
	}
	/**
	 * 保存图像
	 */
	@Log(title = "图面化信息", businessType = BusinessType.UPDATE)
	@PostMapping("/updateAvatar")
	@ResponseBody
	public AjaxResult updateAvatar(MultipartFile file, Zhengqu zq) {
		try {
			zq.setEditorBy(ShiroUtils.getLoginName());
			return toAjax(zhengquService.updateZhengquImg(zq));

           /* if (file != null && !file.isEmpty()) {
//				if (zhengquService.updateZhengqu(zhengqu) > 0)
//				{
//					setSysUser(zhengquService.selectZhengquById(zhengqu.getUserId()));
//					return success();
//				}
            } else {
                return error();
            }*/
		} catch (Exception e) {
			log.error("修改头像失败！", e);
			return error(e.getMessage());
		}
	}

	/**
	 * 审批交通
	 */
	@GetMapping("/shenpi/{zqId}")
	public String shenpi(@PathVariable("zqId") Integer zqId, ModelMap mmap) {
		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		mmap.put("zhengqu", zhengqu);
		return prefix + "/shenpi";
	}

	/**
	 * 审批保存交通
	 */
	@Log(title = "交通", businessType = BusinessType.UPDATE)
	@PostMapping("/shenpiSave")
	@ResponseBody
	public AjaxResult shenpiSave(Zhengqu zhengqu) throws IOException {
		SysNotice notice = new SysNotice();
		notice.setNoticeType(zhengqu.getZqStatus());
		Zhengqu jt = zhengquService.selectZhengquById(zhengqu.getZqId());
		notice.setNoticeTitle("标题为" + "【" + jt.getZqTitle() + "】" + "的地图现势资料信息"+ "：审批通过" );
		websc.sendInfo(JSONObject.toJSONString(notice));
		return toAjax(zhengquService.updateZhengquShenpi(zhengqu));
	}

	/**
	 * 预览审批交通
	 */
	@GetMapping("/shenpiinfo/{zqId}")
	public String shenpiinfo(@PathVariable("zqId") Integer zqId, ModelMap mmap) {
		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		mmap.put("zhengqu", zhengqu);
		return prefix + "/shenpiinfo";
	}

	/**
	 * 预览图面化交通
	 */
	@GetMapping("/avatarinfo/{zqId}")
	public String avatarinfo(@PathVariable("zqId") Integer zqId, ModelMap mmap) {
		Zhengqu zhengqu = zhengquService.selectZhengquById(zqId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String editordateString = formatter.format(zhengqu.getEditorTime());
		zhengqu.setNeweditorTime(editordateString);
		mmap.put("zhengqu", zhengqu);
		return prefix + "/avatarinfo";
	}
}
