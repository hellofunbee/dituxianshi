package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.support.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ZhengquMapper;
import com.ruoyi.system.domain.Zhengqu;
import com.ruoyi.system.service.IZhengquService;

/**
 * 区划地名 服务层实现
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Service
public class ZhengquServiceImpl implements IZhengquService 
{
	@Autowired
	private ZhengquMapper zhengquMapper;

	/**
     * 查询区划地名信息
     * 
     * @param zqId 区划地名ID
     * @return 区划地名信息
     */
    @Override
	public Zhengqu selectZhengquById(Integer zqId)
	{
	    return zhengquMapper.selectZhengquById(zqId);
	}
	
	/**
     * 查询区划地名列表
     * 
     * @param zhengqu 区划地名信息
     * @return 区划地名集合
     */
	@Override
	public List<Zhengqu> selectZhengquList(Zhengqu zhengqu)
	{
	    return zhengquMapper.selectZhengquList(zhengqu);
	}
	
    /**
     * 新增区划地名
     * 
     * @param zhengqu 区划地名信息
     * @return 结果
     */
	@Override
	public int insertZhengqu(Zhengqu zhengqu)
	{
	    return zhengquMapper.insertZhengqu(zhengqu);
	}
	
	/**
     * 修改区划地名
     * 
     * @param zhengqu 区划地名信息
     * @return 结果
     */
	@Override
	public int updateZhengqu(Zhengqu zhengqu)
	{
	    return zhengquMapper.updateZhengqu(zhengqu);
	}

	/**
	 * 修改区划地名
	 *
	 * @param zhengqu 区划地名信息
	 * @return 结果
	 */
	@Override
	public int updateZhengquRoleName(Zhengqu zhengqu)
	{
		return zhengquMapper.updateZhengquRoleName(zhengqu);
	}

	/**
     * 删除区划地名对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteZhengquByIds(String ids)
	{
		return zhengquMapper.deleteZhengquByIds(Convert.toStrArray(ids));
	}

	/**
	 * 图面化交通
	 *
	 * @param zhengqu 交通信息
	 * @return 结果
	 */
	@Override
	public int updateZhengquImg(Zhengqu zhengqu)
	{
		return zhengquMapper.updateZhengquImg(zhengqu);
	}

	/**
	 * 审批交通
	 *
	 * @param zhengqu 交通信息
	 * @return 结果
	 */
	@Override
	public int updateZhengquShenpi(Zhengqu zhengqu)
	{
		return zhengquMapper.updateZhengquShenpi(zhengqu);
	}
	
}
