package com.ruoyi.system.domain;

import com.ruoyi.common.base.BaseEntity;

/**
 * 图书表 cms_book
 *
 * @author ruoyi
 * @date 2019-02-21
 */
public class SearchDomain extends BaseEntity {
    private static final long serialVersionUID = 1L;
    private Long deptId;
    private Integer parentId;
    private String keyWords;
    private String endDate;
    private String beginDate;

    private String tags;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
