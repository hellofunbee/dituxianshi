package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.base.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 交通表 jiaotong
 *
 * @author ruoyi
 * @date 2019-03-21
 */
public class Jiaotong extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 交通 */
	@Excel(name = "交通序号")
	private Integer jtId;
	/** 标题 */
	@Excel(name = "标题")
	private String jtTitle;
	/** 变更日期 */
	@Excel(name = "变更日期",dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date jtChangedate;
	/** 信息来源 */
	@Excel(name = "信息来源")
	private String jtOriginal;
	/** 一级类别 */
	@Excel(name = "一级类别")
	private String class1;
	/** 一级类别 */
	private String jtLb1id;
	/** 二级类别 */
	@Excel(name = "二级类别")
	private String class2;
	private String jtLb2id;
	/** 三级类别 */
	@Excel(name = "三级类别")
	private String class3;
	private String jtLb3id;
	/** 所在省 */
	@Excel(name = "所在省")
	private String sheng;
	private String jtProvinceid;
	/** 所在市 */
	@Excel(name = "所在市")
	private String shi;
	private String jtCityid;
	/** 所在县 */
	@Excel(name = "所在县")
	private String xian;
	private String jtCountryid;
	/** 总名称 */
	@Excel(name = "总名称")
	private String jtZongmc;
	/** 总编号 */
	@Excel(name = "总编号")
	private String jtZongbh;
	/** 分段名称 */
	@Excel(name = "分段名称")
	private String jtFendmc;
	/** 分段编号 */
	@Excel(name = "分段编号")
	private String jtFendbh;
	/** 级别 */
	@Excel(name = "级别")
	private String jtLevels;
	/** 里程 */
	@Excel(name = "里程")
	private BigDecimal jtLicheng;
	/** 详细内容 */
	@Excel(name = "详细内容")
	private String jtDetails;
	/** 图片路径 */
	private String avatar;
	/** 审批内容 */
	private String jtShenpi;

	private String lb1Name;
	/** 二级类别 */
	private String lb2Name;
	/** 三级类别 */
	private String lb3Name;

	/** 新变更日期 */
	private String jtNewChangedate;

	/** 登录角色 */
	private String jtRoleName;

	/** 部门对象 */
//	@Excel(name = "部门名称", targetAttr = "deptName", type = Excel.Type.EXPORT)
	private SysCategory category;
	private SysArea area;
//	@Excel(name = "部门名称", targetAttr = "deptName", type = Excel.Type.EXPORT)
//	private List<SysCategory> category;

	/** 地图数据库 */
	@Excel(name = "图片上传情况")
	private String jtProcess;
	/** 审批状态 */
	@Excel(name = "审批状态")
	private String jtStatus;

	/** 地图院数据库审批 */
	@Excel(name = "地图院数据库审批状态", readConverterExp = "-1=未选中,0=未选中,1=请修改,2=已修改,3=审核通过")
	private Integer jt_ck;

	public SysCategory getCategory() {
		return category;
	}

	public void setCategory(SysCategory category) {
		this.category = category;
	}


//	public List<SysCategory> getCategory() {
//		return category;
//	}
//
//	public void setCategory(List<SysCategory> category) {
//		this.category = category;
//	}

	public SysArea getArea() {
		return area;
	}

	public void setArea(SysArea area) {
		this.area = area;
	}

	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getClass3() {
		return class3;
	}

	public void setClass3(String class3) {
		this.class3 = class3;
	}

	public String getSheng() {
		return sheng;
	}

	public void setSheng(String sheng) {
		this.sheng = sheng;
	}

	public String getShi() {
		return shi;
	}

	public void setShi(String shi) {
		this.shi = shi;
	}

	public String getXian() {
		return xian;
	}

	public void setXian(String xian) {
		this.xian = xian;
	}

	public Integer getJtId() {
		return jtId;
	}

	public void setJtId(Integer jtId) {
		this.jtId = jtId;
	}

	public String getJtTitle() {
		return jtTitle;
	}

	public void setJtTitle(String jtTitle) {
		this.jtTitle = jtTitle;
	}

	public Date getJtChangedate() {
		return jtChangedate;
	}

	public void setJtChangedate(Date jtChangedate) {
		this.jtChangedate = jtChangedate;
	}

	public String getJtProcess() {
		return jtProcess;
	}

	public void setJtProcess(String jtProcess) {
		this.jtProcess = jtProcess;
	}

	public String getJtStatus() {
		return jtStatus;
	}

	public void setJtStatus(String jtStatus) {
		this.jtStatus = jtStatus;
	}

	public String getJtOriginal() {
		return jtOriginal;
	}

	public void setJtOriginal(String jtOriginal) {
		this.jtOriginal = jtOriginal;
	}

	public String getJtLb1id() {
		return jtLb1id;
	}

	public void setJtLb1id(String jtLb1id) {
		this.jtLb1id = jtLb1id;
	}

	public String getJtLb2id() {
		return jtLb2id;
	}

	public void setJtLb2id(String jtLb2id) {
		this.jtLb2id = jtLb2id;
	}

	public String getJtLb3id() {
		return jtLb3id;
	}

	public void setJtLb3id(String jtLb3id) {
		this.jtLb3id = jtLb3id;
	}

	public String getJtProvinceid() {
		return jtProvinceid;
	}

	public void setJtProvinceid(String jtProvinceid) {
		this.jtProvinceid = jtProvinceid;
	}

	public String getJtCityid() {
		return jtCityid;
	}

	public void setJtCityid(String jtCityid) {
		this.jtCityid = jtCityid;
	}

	public String getJtCountryid() {
		return jtCountryid;
	}

	public void setJtCountryid(String jtCountryid) {
		this.jtCountryid = jtCountryid;
	}

	public String getJtZongmc() {
		return jtZongmc;
	}

	public void setJtZongmc(String jtZongmc) {
		this.jtZongmc = jtZongmc;
	}

	public String getJtZongbh() {
		return jtZongbh;
	}

	public void setJtZongbh(String jtZongbh) {
		this.jtZongbh = jtZongbh;
	}

	public String getJtFendmc() {
		return jtFendmc;
	}

	public void setJtFendmc(String jtFendmc) {
		this.jtFendmc = jtFendmc;
	}

	public String getJtFendbh() {
		return jtFendbh;
	}

	public void setJtFendbh(String jtFendbh) {
		this.jtFendbh = jtFendbh;
	}

	public String getJtLevels() {
		return jtLevels;
	}

	public void setJtLevels(String jtLevels) {
		this.jtLevels = jtLevels;
	}

	public BigDecimal getJtLicheng() {
		return jtLicheng;
	}

	public void setJtLicheng(BigDecimal jtLicheng) {
		this.jtLicheng = jtLicheng;
	}

	public String getJtDetails() {
		return jtDetails;
	}

	public void setJtDetails(String jtDetails) {
		this.jtDetails = jtDetails;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getJtShenpi() {
		return jtShenpi;
	}

	public void setJtShenpi(String jtShenpi) {
		this.jtShenpi = jtShenpi;
	}

	public String getLb1Name() {
		return lb1Name;
	}

	public void setLb1Name(String lb1Name) {
		this.lb1Name = lb1Name;
	}

	public String getLb2Name() {
		return lb2Name;
	}

	public void setLb2Name(String lb2Name) {
		this.lb2Name = lb2Name;
	}

	public String getLb3Name() {
		return lb3Name;
	}

	public void setLb3Name(String lb3Name) {
		this.lb3Name = lb3Name;
	}


	public String getJtNewChangedate() {
		return jtNewChangedate;
	}

	public void setJtNewChangedate(String jtNewChangedate) {
		this.jtNewChangedate = jtNewChangedate;
	}

	public String getJtRoleName() {
		return jtRoleName;
	}

	public void setJtRoleName(String jtRoleName) {
		this.jtRoleName = jtRoleName;
	}

	public Integer getJt_ck() {
		return jt_ck;
	}

	public void setJt_ck(Integer jt_ck) {
		this.jt_ck = jt_ck;
	}

}
