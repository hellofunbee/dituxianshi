Handlebars.registerHelper({
    'prettifyDate': function (timestamp) {//格式化时间
        var format = 'YYYY-MM-DD';
        if (arguments.length > 2) {
            format = arguments[1];
        }
        if (timestamp) {
            return moment(new Date(timestamp)).format(format);
        } else {
            return '';
        }
    },//格式化数字默认空为''
    'prettifyNumber': function (number) {
        var format = '0.00', zero = true;
        if (arguments.length > 2) {
            format = arguments[1];
        }
        if (arguments.length > 3) {
            zero = arguments[2];
        }

        return (zero ? number != null : !!number) ? numeral(number).format(format) : '';

    },//字符去空格
    'prettifyStr': function (s) {
        return s && s.replace(/\s/g, " ");
        ;

    },//格式化数字默认空为0
    'prettifyPositive': function (number) {
        var format = '0.00', zero = true;
        if (arguments.length > 2) {
            format = arguments[1];
        }
        if (arguments.length > 3) {
            zero = arguments[2];
        }

        return (zero ? number != null && number > 0 : !!number) ? numeral(number).format(format) : '0';

    },//减
    'subtract': function (number1, number2) {
        var format = '0.00';
        if (arguments.length > 3) {
            format = arguments[2];
        }
        var number = number1 - number2;
        return number ? numeral(number).format(format) : '';
    },//加
    'computeAdd': function () {
        var big = 0;
        try {
            var len = arguments.length - 1;
            for (var i = 0; i < len; i++) {
                if (arguments[i]) {
                    big = eval(big + "+" + arguments[i]);
                }
            }
        } catch (e) {
            throw new Error('Handlerbars Helper "computeAdd" can not deal with wrong expression:' + arguments);
        }
        return big;
    }, 'getSimpleText': function (str) {
        if (str) {
            <!--<$[信息内容]>begin-->
            str = str.replace("begin-->", '')
            str = str.replace(/<\/?[^>]*>/g, ''); //去除HTML tag
            str = str.replace(/[ | ]*\n/g, '\n'); //去除行尾空白
            //str = str.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
            str = str.replace(/ /ig, '');//去掉

            if (str.length > 230) {
                str = str.substring(0, 220) + '...'
            }

        }

        return str;
    }, 'fastCapture': function (fFile,fInfoId) {
        if (!fFile) {
            return '<span style="color: #999">暂无快照</span>'
        } else {
            return '<a target="_blank" href="/out/cmsInfoDetail/capture/' + fInfoId + '">快照</a>'
        }
    }
});