package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Shuixi;
import java.util.List;	

/**
 * 水系 数据层
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public interface ShuixiMapper 
{
	/**
     * 查询水系信息
     * 
     * @param sxId 水系ID
     * @return 水系信息
     */
	public Shuixi selectShuixiById(Integer sxId);
	
	/**
     * 查询水系列表
     * 
     * @param shuixi 水系信息
     * @return 水系集合
     */
	public List<Shuixi> selectShuixiList(Shuixi shuixi);
	
	/**
     * 新增水系
     * 
     * @param shuixi 水系信息
     * @return 结果
     */
	public int insertShuixi(Shuixi shuixi);
	
	/**
     * 修改水系
     * 
     * @param shuixi 水系信息
     * @return 结果
     */
	public int updateShuixi(Shuixi shuixi);


	/**
	 * 修改水系
	 *
	 * @param shuixi 水系信息
	 * @return 结果
	 */
	public int updateShuixiRoleName(Shuixi shuixi);
	/**
     * 删除水系
     * 
     * @param sxId 水系ID
     * @return 结果
     */
	public int deleteShuixiById(Integer sxId);
	
	/**
     * 批量删除水系
     * 
     * @param sxIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteShuixiByIds(String[] sxIds);

	/**
	 * 图面化交通
	 *
	 * @param shuixi 交通信息
	 * @return 结果
	 */
	public int updateShuixiImg(Shuixi shuixi);

	/**
	 * 审批交通
	 *
	 * @param shuixi 交通信息
	 * @return 结果
	 */
	public int updateShuixiShenpi(Shuixi shuixi);
}