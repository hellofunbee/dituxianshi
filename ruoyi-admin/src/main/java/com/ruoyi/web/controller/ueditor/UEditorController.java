package com.ruoyi.web.controller.ueditor;

import com.baidu.ueditor.ActionEnter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class UEditorController {

	@RequestMapping("/ueditor")
	private String showPage() {
		return "ueditor";
	}

	@RequestMapping(value="/config")
	public void config(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json");  
		String rootPath = request.getSession().getServletContext().getRealPath("/");
//		 rootPath = UEditorController.class.getClassLoader().getResource("config.json").getPath();
//		 rootPath = new File(rootPath).getParent();
		try {
			String exec = new ActionEnter(request, rootPath).exec();
			PrintWriter writer = response.getWriter();  
			writer.write(exec);  
			writer.flush();  
			writer.close();  
		} catch (IOException e) {  
			e.printStackTrace();  
		}  
	}
}
