package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.SysDept;
import com.ruoyi.system.domain.SysRole;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysDictType1;
import com.ruoyi.system.service.ISysDictTypeService1;
import com.ruoyi.framework.web.base.BaseController;

/**
 * 数据字典信息
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/dict1")
public class SysDictTypeController1 extends BaseController
{
    private String prefix = "system/dict1/type";

    @Autowired
    private ISysDictTypeService1 dictTypeService;

    @RequiresPermissions("system:dict1:view")
    @GetMapping()
    public String dictType()
    {
        return prefix + "/type";
    }

    @RequestMapping("/list")
    @RequiresPermissions("system:dict1:list")
    @ResponseBody
    public TableDataInfo list(SysDictType1 dictType)
    {
        startPage();
        List<SysDictType1> list = dictTypeService.selectDictTypeList(dictType);
        return getDataTable(list);
    }

    @Log(title = "字典类型", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:dict1:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysDictType1 dictType)
    {

        List<SysDictType1> list = dictTypeService.selectDictTypeList(dictType);
        ExcelUtil<SysDictType1> util = new ExcelUtil<SysDictType1>(SysDictType1.class);
        return util.exportExcel(list, "字典类型");
    }

    /**
     * 新增字典类型
     */
    //  @GetMapping("/add")
    // public String add()
    // {
    //   return prefix + "/add";
    //}

    @GetMapping("/add/{dt_parentId}")
    public String add(@PathVariable("dt_parentId") Long dt_parentId, ModelMap mmap)
    {
        mmap.put("dict", dictTypeService.selectDictTypeById(dt_parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存字典类型
     */
    @Log(title = "字典类型", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:dict1:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysDictType1 dict)
    {
        dict.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(dictTypeService.insertDictType(dict));
    }

    /**
     * 修改字典类型
     */
    @GetMapping("/edit/{dictId}")
    public String edit(@PathVariable("dictId") Long dictId, ModelMap mmap)
    {
        mmap.put("dict", dictTypeService.selectDictTypeById(dictId));
        return prefix + "/edit";
    }

    /**
     * 修改保存字典类型
     */
    @Log(title = "字典类型", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:dict1:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysDictType1 dict)
    {
        dict.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(dictTypeService.updateDictType(dict));
    }

    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:dict1:remove")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        try
        {
            return toAjax(dictTypeService.deleteDictTypeByIds(ids));
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    }

    /**
     * 查询字典详细
     */
    @RequiresPermissions("system:dict1:list")
    @GetMapping("/detail/{dictId}")
    public String detail(@PathVariable("dictId") Long dictId, ModelMap mmap)
    {
        mmap.put("dict", dictTypeService.selectDictTypeById(dictId));
        mmap.put("dictList", dictTypeService.selectDictTypeAll());
        return "system/dict1/data/data";
    }

    /**
     * 校验字典类型
     */
    @PostMapping("/checkDictTypeUnique")
    @ResponseBody
    public String checkDictTypeUnique(SysDictType1 dictType)
    {
        return dictTypeService.checkDictTypeUnique(dictType);
    }

    /**
     * 选择字典树
     */
    @GetMapping("/selectDictTypeTree/{dictId}")
    public String selectDictTypeTree(@PathVariable("dictId") Long dictId, ModelMap mmap)
    {
        mmap.put("dict", dictTypeService.selectDictTypeById(dictId));
        return prefix + "/tree";
    }

    /**
     * 加载部门列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Map<String, Object>> treeData()
    {
        List<Map<String, Object>> tree = dictTypeService.selectDictTypeTree(new SysDictType1());
        return tree;
    }

}
