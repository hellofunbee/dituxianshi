package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.Lvyou;
import com.ruoyi.system.domain.Shuixi;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.service.ILvyouService;
import com.ruoyi.system.service.ISysCategoryService;
import com.ruoyi.system.util.MyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 旅游 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Controller
@RequestMapping("/system/lvyou")
public class LvyouController extends BaseController
{
	private static final Logger log = LoggerFactory.getLogger(SysProfileController.class);
    private String prefix = "system/lvyou";
	@Autowired
	private MyWebScoketController websc;
	@Autowired
	private ISysCategoryService categoryService;
	@Autowired
	private ILvyouService lvyouService;
	
	@RequiresPermissions("system:lvyou:view")
	@GetMapping()
	public String lvyou()
	{
	    return prefix + "/lvyou";
	}
	
	/**
	 * 查询旅游列表
	 */
	@RequiresPermissions("system:lvyou:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Lvyou lvyou)
	{
		startPage();
		lvyou.setLvRoleName(ShiroUtils.getSysUser().getUserName());
		lvyouService.updateLvyouRoleName(lvyou);
		lvyou.setLvTitle(MyUtil.getSql(lvyou.getLvTitle(),"lv_title"));
		lvyou.setLvDetails(MyUtil.getSql(lvyou.getLvDetails(),"lv_details"));
        List<Lvyou> list = lvyouService.selectLvyouList(lvyou);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出旅游列表
	 */
	@RequiresPermissions("system:lvyou:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Lvyou lvyou)
    {
		lvyou.setLvTitle(MyUtil.getSql(lvyou.getLvTitle(),"lv_title"));
		lvyou.setLvDetails(MyUtil.getSql(lvyou.getLvDetails(),"lv_details"));

		List<Lvyou> list = lvyouService.selectLvyouList(lvyou);
        ExcelUtil<Lvyou> util = new ExcelUtil<Lvyou>(Lvyou.class);
        return util.exportExcel(list, "旅游信息");
    }
	
	/**
	 * 新增旅游
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存旅游
	 */
	@RequiresPermissions("system:lvyou:add")
	@Log(title = "旅游", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Lvyou lvyou)
	{
		lvyou.setCreateBy(ShiroUtils.getLoginName());
		return toAjax(lvyouService.insertLvyou(lvyou));
	}

	/**
	 * 修改旅游
	 */
	@GetMapping("/edit/{lvId}")
	public String edit(@PathVariable("lvId") Integer lvId, ModelMap mmap)
	{
		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(lvyou.getLvChangedate()!=null) {
			String changedateString = formatter.format(lvyou.getLvChangedate());
			lvyou.setLvNewChangedate(changedateString);
		}
		mmap.put("lvyou", lvyou);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存旅游
	 */
	@RequiresPermissions("system:lvyou:edit")
	@Log(title = "旅游", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Lvyou lvyou)
	{
		lvyou.setUpdateBy(ShiroUtils.getLoginName());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date currentTime_2  = formatter.parse(lvyou.getLvNewChangedate(),pos);
		lvyou.setLvChangedate(currentTime_2);
		return toAjax(lvyouService.updateLvyou(lvyou));
	}
	
	/**
	 * 删除旅游
	 */
	@RequiresPermissions("system:lvyou:remove")
	@Log(title = "旅游", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(lvyouService.deleteLvyouByIds(ids));
	}

	/**
	 * 预览旅游
	 */
	@GetMapping("/info/{lvId}")
	public String info(@PathVariable("lvId") Integer lvId, ModelMap mmap)
	{
		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(lvyou.getCreateTime());
		if(lvyou.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(lvyou.getUpdateTime());
			lvyou.setNewupdateTime(updatedateString);
		}
		if(lvyou.getLvChangedate()!=null) {
			String changedateString = formatter.format(lvyou.getLvChangedate());
			lvyou.setLvNewChangedate(changedateString);
		}
		lvyou.setNewcreateTime(createdateString);

		mmap.put("lvyou", lvyou);
		return prefix + "/info";
	}

	/**
	 * 预览旅游
	 */
	@GetMapping("/infoFront/{lvId}")
	public String infoFront(@PathVariable("lvId") Integer lvId, ModelMap mmap, SysCategory category, String title)
	{

		SysCategory categoryNew = new SysCategory();

		if (category == null) {
			category = new SysCategory();
		}

		//所有的节点
		SysCategory c = new SysCategory();
		c.setParentId(100L);
		List<SysCategory> categoryList = categoryService.selectDeptList(c);

		//子目录
		List<SysCategory> childs = new ArrayList<>();

		if (category.getLevel() == null || category.getLevel() == 1) {
			if (category.getDeptId() == null) {
				categoryNew.setParentId(categoryList.get(0).getDeptId());
			} else {
				for (SysCategory n : categoryList) {
					if (n.getDeptId() == category.getDeptId()) {
						n.setCurr(true);
						categoryNew.setParentId(n.getDeptId());
					}
				}
			}
			childs = categoryService.selectDeptList(categoryNew);

		} else if (category.getLevel() == 2) {
			category = categoryService.selectDeptById(category.getDeptId());
			category.setCurr(true);
			categoryNew.setParentId(category.getParentId());
			//选中父级
			for (SysCategory n : categoryList) {
				if (n.getDeptId() == category.getParentId()) {
					n.setCurr(true);
				}
			}
			childs = categoryService.selectDeptList(categoryNew);
			//选中子级
			for (SysCategory c1 : childs) {
				if (c1.getDeptId() == category.getDeptId()) {
					System.out.println(c1.getDeptId() + "=="+ category.getDeptId());
					c1.setCurr(true);
				}
			}
		}
		mmap.put("nodes", categoryList);
		mmap.put("title", title);
		mmap.put("curr", categoryNew);
		mmap.put("curr_child", category);
		mmap.put("childs", childs);


		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(lvyou.getCreateTime());
		if(lvyou.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(lvyou.getUpdateTime());
			lvyou.setNewupdateTime(updatedateString);
		}
		if(lvyou.getLvChangedate()!=null) {
			String changedateString = formatter.format(lvyou.getLvChangedate());
			lvyou.setLvNewChangedate(changedateString);
		}
		lvyou.setNewcreateTime(createdateString);

		mmap.put("lvyou", lvyou);
		return prefix + "/infoFront";
	}

	/**
	 * 图面化
	 */
	@GetMapping("/avatar/{lvId}")
	public String avatar(@PathVariable("lvId") Integer lvId, ModelMap mmap) {
		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		mmap.put("lvyou", lvyou);
		return prefix + "/avatar";
	}
	/**
	 * 保存图像
	 */
	@Log(title = "图面化信息", businessType = BusinessType.UPDATE)
	@PostMapping("/updateAvatar")
	@ResponseBody
	public AjaxResult updateAvatar(MultipartFile file, Lvyou lv) {
		try {
			lv.setEditorBy(ShiroUtils.getLoginName());
			return toAjax(lvyouService.updateLvyouImg(lv));

           /* if (file != null && !file.isEmpty()) {
//				if (lvyouService.updateLvyou(lvyou) > 0)
//				{
//					setSysUser(lvyouService.selectLvyouById(lvyou.getUserId()));
//					return success();
//				}
            } else {
                return error();
            }*/
		} catch (Exception e) {
			log.error("修改头像失败！", e);
			return error(e.getMessage());
		}
	}

	/**
	 * 审批交通
	 */
	@GetMapping("/shenpi/{lvId}")
	public String shenpi(@PathVariable("lvId") Integer lvId, ModelMap mmap) {
		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		mmap.put("lvyou", lvyou);
		return prefix + "/shenpi";
	}

	/**
	 * 审批保存交通
	 */
	@Log(title = "交通", businessType = BusinessType.UPDATE)
	@PostMapping("/shenpiSave")
	@ResponseBody
	public AjaxResult shenpiSave(Lvyou lvyou) throws IOException {
		SysNotice notice = new SysNotice();
		notice.setNoticeType(lvyou.getLvStatus());
		Lvyou jt = lvyouService.selectLvyouById(lvyou.getLvId());
		notice.setNoticeTitle("标题为" + "【" + jt.getLvTitle() + "】" + "的地图现势资料信息"+ "：审批通过" );
		websc.sendInfo(JSONObject.toJSONString(notice));
		return toAjax(lvyouService.updateLvyouShenpi(lvyou));
	}

	/**
	 * 预览审批交通
	 */
	@GetMapping("/shenpiinfo/{lvId}")
	public String shenpiinfo(@PathVariable("lvId") Integer lvId, ModelMap mmap) {
		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		mmap.put("lvyou", lvyou);
		return prefix + "/shenpiinfo";
	}

	/**
	 * 预览图面化交通
	 */
	@GetMapping("/avatarinfo/{lvId}")
	public String avatarinfo(@PathVariable("lvId") Integer lvId, ModelMap mmap) {
		Lvyou lvyou = lvyouService.selectLvyouById(lvId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String editordateString = formatter.format(lvyou.getEditorTime());
		lvyou.setNeweditorTime(editordateString);
		mmap.put("lvyou", lvyou);
		return prefix + "/avatarinfo";
	}
}
