package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.base.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;

/**
 * 综合表 zonghe
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public class Zonghe extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 综合 */
	@Excel(name = "综合序号")
	private Integer zhId;
	/** 标题 */
	@Excel(name = "标题")
	private String zhTitle;
	/** 变更日期 */
	@Excel(name = "变更日期",dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date zhChangedate;
	/** 信息来源 */
	@Excel(name = "信息来源")
	private String zhOriginal;

	@Excel(name = "一级类别")
	private String class1;
	@Excel(name = "二级类别")
	private String class2;
	@Excel(name = "三级类别")
	private String class3;
	@Excel(name = "所在省")
	private String sheng;
	@Excel(name = "所在市")
	private String shi;
	@Excel(name = "所在县")
	private String xian;
	private SysCategory category;
	private SysArea area;



	/** 一级类别 */
	private String zhLb1;
	/** 二级类别 */
	private String zhLb2;
	/** 三级类别 */
	private String zhLb3;
	/** 所在省 */
	private String zhProvince;
	/** 所在市 */
	private String zhCity;
	/** 所在县 */
	private String zhCountry;
	/** 批次 */
	@Excel(name = "批次")
	private String zhPici;
	/** 审批机构 */
	@Excel(name = "审批机构")
	private String zhShenpijc;
	/** 级别 */
	@Excel(name = "级别")
	private String zhLevels;
	/** 详细内容 */
	@Excel(name = "详细内容")
	private String zhDetails;
	/** 图片保存路径 */
	private String avatar;
	/** 审批内容 */
	private String zhShenpi;
	/** 新变更日期 */
	private String zhNewChangedate;
	/** 登录角色 */
	private String zhRoleName;

	/** 地图数据库 */
	@Excel(name = "图片上传情况")
	private String zhProcess;
	/** 审批状态 */
	@Excel(name = "审批状态")
	private String zhStatus;

	/** 地图院数据库审批 */
	@Excel(name = "地图院数据库审批状态", readConverterExp = "-1=未选中,0=未选中,1=请修改,2=已修改,3=审核通过")
	private Integer jt_ck;

	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getClass3() {
		return class3;
	}

	public void setClass3(String class3) {
		this.class3 = class3;
	}

	public String getSheng() {
		return sheng;
	}

	public void setSheng(String sheng) {
		this.sheng = sheng;
	}

	public String getShi() {
		return shi;
	}

	public void setShi(String shi) {
		this.shi = shi;
	}

	public String getXian() {
		return xian;
	}

	public void setXian(String xian) {
		this.xian = xian;
	}

	public SysCategory getCategory() {
		return category;
	}

	public void setCategory(SysCategory category) {
		this.category = category;
	}

	public SysArea getArea() {
		return area;
	}

	public void setArea(SysArea area) {
		this.area = area;
	}

	public String getZhRoleName() {
		return zhRoleName;
	}

	public void setZhRoleName(String zhRoleName) {
		this.zhRoleName = zhRoleName;
	}

	public String getZhNewChangedate() {
		return zhNewChangedate;
	}

	public void setZhNewChangedate(String zhNewChangedate) {
		this.zhNewChangedate = zhNewChangedate;
	}

	public Integer getZhId() {
		return zhId;
	}

	public void setZhId(Integer zhId) {
		this.zhId = zhId;
	}

	public String getZhTitle() {
		return zhTitle;
	}

	public void setZhTitle(String zhTitle) {
		this.zhTitle = zhTitle;
	}

	public Date getZhChangedate() {
		return zhChangedate;
	}

	public void setZhChangedate(Date zhChangedate) {
		this.zhChangedate = zhChangedate;
	}

	public String getZhProcess() {
		return zhProcess;
	}

	public void setZhProcess(String zhProcess) {
		this.zhProcess = zhProcess;
	}

	public String getZhStatus() {
		return zhStatus;
	}

	public void setZhStatus(String zhStatus) {
		this.zhStatus = zhStatus;
	}

	public String getZhOriginal() {
		return zhOriginal;
	}

	public void setZhOriginal(String zhOriginal) {
		this.zhOriginal = zhOriginal;
	}

	public String getZhLb1() {
		return zhLb1;
	}

	public void setZhLb1(String zhLb1) {
		this.zhLb1 = zhLb1;
	}

	public String getZhLb2() {
		return zhLb2;
	}

	public void setZhLb2(String zhLb2) {
		this.zhLb2 = zhLb2;
	}

	public String getZhLb3() {
		return zhLb3;
	}

	public void setZhLb3(String zhLb3) {
		this.zhLb3 = zhLb3;
	}

	public String getZhProvince() {
		return zhProvince;
	}

	public void setZhProvince(String zhProvince) {
		this.zhProvince = zhProvince;
	}

	public String getZhCity() {
		return zhCity;
	}

	public void setZhCity(String zhCity) {
		this.zhCity = zhCity;
	}

	public String getZhCountry() {
		return zhCountry;
	}

	public void setZhCountry(String zhCountry) {
		this.zhCountry = zhCountry;
	}

	public String getZhPici() {
		return zhPici;
	}

	public void setZhPici(String zhPici) {
		this.zhPici = zhPici;
	}

	public String getZhShenpijc() {
		return zhShenpijc;
	}

	public void setZhShenpijc(String zhShenpijc) {
		this.zhShenpijc = zhShenpijc;
	}

	public String getZhLevels() {
		return zhLevels;
	}

	public void setZhLevels(String zhLevels) {
		this.zhLevels = zhLevels;
	}

	public String getZhDetails() {
		return zhDetails;
	}

	public void setZhDetails(String zhDetails) {
		this.zhDetails = zhDetails;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getZhShenpi() {
		return zhShenpi;
	}

	public void setZhShenpi(String zhShenpi) {
		this.zhShenpi = zhShenpi;
	}

    public void setJt_ck(Integer jt_ck) {
        this.jt_ck = jt_ck;
    }

    public Integer getJt_ck() {
        return jt_ck;
    }
}
