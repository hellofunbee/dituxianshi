package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.base.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;

/**
 * 旅游表 lvyou
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public class Lvyou extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 旅游 */
	@Excel(name = "旅游序号")
	private Integer lvId;
	/** 标题 */
	@Excel(name = "标题")
	private String lvTitle;
	/** 变更日期 */
	@Excel(name = "变更日期",dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date lvChangedate;
	/** 信息来源 */
	@Excel(name = "信息来源")
	private String lvOriginal;

	@Excel(name = "一级类别")
	private String class1;
	@Excel(name = "二级类别")
	private String class2;
	@Excel(name = "三级类别")
	private String class3;
	@Excel(name = "所在省")
	private String sheng;
	@Excel(name = "所在市")
	private String shi;
	@Excel(name = "所在县")
	private String xian;
	private SysCategory category;
	private SysArea area;

	/** 一级类别 */
	private String lvLb1;
	/** 二级类别 */
	private String lvLb2;
	/** 三级类别 */
	private String lvLb3;
	/** 所在省 */
	private String lvProvince;
	/** 所在市 */
	private String lvCity;
	/** 所在县 */
	private String lvCountry;
	/** 批次 */
	@Excel(name = "批次")
	private String lvPici;
	/** 审批机构 */
	@Excel(name = "审批机构")
	private String lvShenpijc;
	/** 级别 */
	@Excel(name = "级别")
	private String lvLevels;
	/** 详细内容 */
	@Excel(name = "详细内容")
	private String lvDetails;
	/** 图片保存路径 */
	private String avatar;
	/** 审批内容 */
	private String lvShenpi;
	/** 新变更日期 */
	private String lvNewChangedate;
	/** 新变更日期 */
	private String lvRoleName;

	/** 地图数据库 */
	@Excel(name = "图片上传情况")
	private String lvProcess;
	/** 审批状态 */
	@Excel(name = "审批状态")
	private String lvStatus;

	/** 地图院数据库审批 */
	@Excel(name = "地图院数据库审批状态", readConverterExp = "-1=未选中,0=未选中,1=请修改,2=已修改,3=审核通过")
	private Integer jt_ck;


	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getClass3() {
		return class3;
	}

	public void setClass3(String class3) {
		this.class3 = class3;
	}

	public String getSheng() {
		return sheng;
	}

	public void setSheng(String sheng) {
		this.sheng = sheng;
	}

	public String getShi() {
		return shi;
	}

	public void setShi(String shi) {
		this.shi = shi;
	}

	public String getXian() {
		return xian;
	}

	public void setXian(String xian) {
		this.xian = xian;
	}

	public SysCategory getCategory() {
		return category;
	}

	public void setCategory(SysCategory category) {
		this.category = category;
	}

	public SysArea getArea() {
		return area;
	}

	public void setArea(SysArea area) {
		this.area = area;
	}

	public String getLvRoleName() {
		return lvRoleName;
	}

	public void setLvRoleName(String lvRoleName) {
		this.lvRoleName = lvRoleName;
	}

	public String getLvNewChangedate() {
		return lvNewChangedate;
	}

	public void setLvNewChangedate(String lvNewChangedate) {
		this.lvNewChangedate = lvNewChangedate;
	}

	public Integer getLvId() {
		return lvId;
	}

	public void setLvId(Integer lvId) {
		this.lvId = lvId;
	}

	public String getLvTitle() {
		return lvTitle;
	}

	public void setLvTitle(String lvTitle) {
		this.lvTitle = lvTitle;
	}

	public Date getLvChangedate() {
		return lvChangedate;
	}

	public void setLvChangedate(Date lvChangedate) {
		this.lvChangedate = lvChangedate;
	}

	public String getLvProcess() {
		return lvProcess;
	}

	public void setLvProcess(String lvProcess) {
		this.lvProcess = lvProcess;
	}

	public String getLvStatus() {
		return lvStatus;
	}

	public void setLvStatus(String lvStatus) {
		this.lvStatus = lvStatus;
	}

	public String getLvOriginal() {
		return lvOriginal;
	}

	public void setLvOriginal(String lvOriginal) {
		this.lvOriginal = lvOriginal;
	}

	public String getLvLb1() {
		return lvLb1;
	}

	public void setLvLb1(String lvLb1) {
		this.lvLb1 = lvLb1;
	}

	public String getLvLb2() {
		return lvLb2;
	}

	public void setLvLb2(String lvLb2) {
		this.lvLb2 = lvLb2;
	}

	public String getLvLb3() {
		return lvLb3;
	}

	public void setLvLb3(String lvLb3) {
		this.lvLb3 = lvLb3;
	}

	public String getLvProvince() {
		return lvProvince;
	}

	public void setLvProvince(String lvProvince) {
		this.lvProvince = lvProvince;
	}

	public String getLvCity() {
		return lvCity;
	}

	public void setLvCity(String lvCity) {
		this.lvCity = lvCity;
	}

	public String getLvCountry() {
		return lvCountry;
	}

	public void setLvCountry(String lvCountry) {
		this.lvCountry = lvCountry;
	}

	public String getLvPici() {
		return lvPici;
	}

	public void setLvPici(String lvPici) {
		this.lvPici = lvPici;
	}

	public String getLvShenpijc() {
		return lvShenpijc;
	}

	public void setLvShenpijc(String lvShenpijc) {
		this.lvShenpijc = lvShenpijc;
	}

	public String getLvLevels() {
		return lvLevels;
	}

	public void setLvLevels(String lvLevels) {
		this.lvLevels = lvLevels;
	}

	public String getLvDetails() {
		return lvDetails;
	}

	public void setLvDetails(String lvDetails) {
		this.lvDetails = lvDetails;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getLvShenpi() {
		return lvShenpi;
	}

	public void setLvShenpi(String lvShenpi) {
		this.lvShenpi = lvShenpi;
	}

    public void setJt_ck(Integer jt_ck) {
        this.jt_ck = jt_ck;
    }

    public Integer getJt_ck() {
        return jt_ck;
    }
}
