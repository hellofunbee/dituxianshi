package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.support.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.JiaotongMapper;
import com.ruoyi.system.domain.Jiaotong;
import com.ruoyi.system.service.IJiaotongService;

/**
 * 交通 服务层实现
 * 
 * @author ruoyi
 * @date 2019-03-21
 */
@Service
public class JiaotongServiceImpl implements IJiaotongService 
{
	@Autowired
	private JiaotongMapper jiaotongMapper;

	/**
     * 查询交通信息
     * 
     * @param jtId 交通ID
     * @return 交通信息
     */
    @Override
	public Jiaotong selectJiaotongById(Integer jtId)
	{
	    return jiaotongMapper.selectJiaotongById(jtId);
	}

	/**
	 * 查询交通信息
	 *
	 * @param jtId 交通ID
	 * @return 交通信息
	 */
	@Override
	public Jiaotong selectJiaotongByIdInfo(Integer jtId)
	{
		return jiaotongMapper.selectJiaotongByIdInfo(jtId);
	}

	/**
     * 查询交通列表
     * 
     * @param jiaotong 交通信息
     * @return 交通集合
     */
	@Override
	public List<Jiaotong> selectJiaotongList(Jiaotong jiaotong)
	{
	    return jiaotongMapper.selectJiaotongList(jiaotong);
	}
	
    /**
     * 新增交通
     * 
     * @param jiaotong 交通信息
     * @return 结果
     */
	@Override
	public int insertJiaotong(Jiaotong jiaotong)
	{
	    return jiaotongMapper.insertJiaotong(jiaotong);
	}
	
	/**
     * 修改交通
     * 
     * @param jiaotong 交通信息
     * @return 结果
     */
	@Override
	public int updateJiaotong(Jiaotong jiaotong)
	{
	    return jiaotongMapper.updateJiaotong(jiaotong);
	}

	/**
     * 删除交通对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteJiaotongByIds(String ids)
	{
		return jiaotongMapper.deleteJiaotongByIds(Convert.toStrArray(ids));
	}

	/**
	 * 图面化交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	@Override
	public int updateJiaotongRoleName(Jiaotong jiaotong)
	{
		return jiaotongMapper.updateJiaotongRoleName(jiaotong);
	}

	/**
	 * 图面化交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	@Override
	public int updateJiaotongImg(Jiaotong jiaotong)
	{
		return jiaotongMapper.updateJiaotongImg(jiaotong);
	}

	/**
	 * 审批交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	@Override
	public int updateJiaotongShenpi(Jiaotong jiaotong)
	{
		return jiaotongMapper.updateJiaotongShenpi(jiaotong);
	}
	
}
