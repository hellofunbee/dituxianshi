package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.support.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ZongheMapper;
import com.ruoyi.system.domain.Zonghe;
import com.ruoyi.system.service.IZongheService;

/**
 * 综合 服务层实现
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Service
public class ZongheServiceImpl implements IZongheService 
{
	@Autowired
	private ZongheMapper zongheMapper;

	/**
     * 查询综合信息
     * 
     * @param zhId 综合ID
     * @return 综合信息
     */
    @Override
	public Zonghe selectZongheById(Integer zhId)
	{
	    return zongheMapper.selectZongheById(zhId);
	}
	
	/**
     * 查询综合列表
     * 
     * @param zonghe 综合信息
     * @return 综合集合
     */
	@Override
	public List<Zonghe> selectZongheList(Zonghe zonghe)
	{
	    return zongheMapper.selectZongheList(zonghe);
	}
	
    /**
     * 新增综合
     * 
     * @param zonghe 综合信息
     * @return 结果
     */
	@Override
	public int insertZonghe(Zonghe zonghe)
	{
	    return zongheMapper.insertZonghe(zonghe);
	}
	
	/**
     * 修改综合
     * 
     * @param zonghe 综合信息
     * @return 结果
     */
	@Override
	public int updateZonghe(Zonghe zonghe)
	{
	    return zongheMapper.updateZonghe(zonghe);
	}

	/**
	 * 修改综合
	 *
	 * @param zonghe 综合信息
	 * @return 结果
	 */
	@Override
	public int updateZongheRoleName(Zonghe zonghe)
	{
		return zongheMapper.updateZongheRoleName(zonghe);
	}

	/**
     * 删除综合对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteZongheByIds(String ids)
	{
		return zongheMapper.deleteZongheByIds(Convert.toStrArray(ids));
	}

	/**
	 * 图面化交通
	 *
	 * @param zonghe 交通信息
	 * @return 结果
	 */
	@Override
	public int updateZongheImg(Zonghe zonghe)
	{
		return zongheMapper.updateZongheImg(zonghe);
	}

	/**
	 * 审批交通
	 *
	 * @param zonghe 交通信息
	 * @return 结果
	 */
	@Override
	public int updateZongheShenpi(Zonghe zonghe)
	{
		return zongheMapper.updateZongheShenpi(zonghe);
	}
}
