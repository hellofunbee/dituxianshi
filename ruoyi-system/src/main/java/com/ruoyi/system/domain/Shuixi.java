package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.base.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;

/**
 * 水系表 shuixi
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public class Shuixi extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 水系 */
	@Excel(name = "水系序号")
	private Integer sxId;
	/** 标题 */
	@Excel(name = "标题")
	private String sxTitle;
	/** 变更日期 */
	@Excel(name = "变更日期",dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date sxChangedate;
	/** 信息来源 */
	@Excel(name = "信息来源")
	private String sxOriginal;

	@Excel(name = "一级类别")
	private String class1;
	@Excel(name = "二级类别")
	private String class2;
	@Excel(name = "三级类别")
	private String class3;
	@Excel(name = "所在省")
	private String sheng;
	@Excel(name = "所在市")
	private String shi;
	@Excel(name = "所在县")
	private String xian;
	private SysCategory category;
	private SysArea area;

	/** 一级类别 */
	private String sxLb1;
	/** 二级类别 */
	private String sxLb2;
	/** 三级类别 */
	private String sxLb3;
	/** 所在省 */
	private String sxProvince;
	/** 所在市 */
	private String sxCity;
	/** 所在县 */
	private String sxCountry;
	/** 批次 */
	@Excel(name = "批次")
	private String sxPici;
	/** 审批机构 */
	@Excel(name = "审批机构")
	private String sxShenpijc;
	/** 级别 */
	@Excel(name = "级别")
	private String sxLevels;
	/** 详细内容 */
	@Excel(name = "详细内容")
	private String sxDetails;
	/** 图片保存路径 */
	private String avatar;
	/** 审批内容 */
	private String sxShenpi;
	/** 新变更日期 */
	private String sxNewChangedate;
	/** 登录角色 */
	private String sxRoleName;

	/** 地图数据库 */
	@Excel(name = "图片上传情况")
	private String sxProcess;
	/** 审批状态 */
	@Excel(name = "审批状态")
	private String sxStatus;

	/** 地图院数据库审批 */
	@Excel(name = "地图院数据库审批状态", readConverterExp = "-1=未选中,0=未选中,1=请修改,2=已修改,3=审核通过")
	private Integer jt_ck;

	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getClass3() {
		return class3;
	}

	public void setClass3(String class3) {
		this.class3 = class3;
	}

	public String getSheng() {
		return sheng;
	}

	public void setSheng(String sheng) {
		this.sheng = sheng;
	}

	public String getShi() {
		return shi;
	}

	public void setShi(String shi) {
		this.shi = shi;
	}

	public String getXian() {
		return xian;
	}

	public void setXian(String xian) {
		this.xian = xian;
	}

	public SysCategory getCategory() {
		return category;
	}

	public void setCategory(SysCategory category) {
		this.category = category;
	}

	public SysArea getArea() {
		return area;
	}

	public void setArea(SysArea area) {
		this.area = area;
	}

	public void setSxRoleName(String sxRoleName) {
		this.sxRoleName = sxRoleName;
	}

	public String getSxRoleName() {
		return sxRoleName;
	}

	public String getSxNewChangedate() {
		return sxNewChangedate;
	}

	public void setSxNewChangedate(String sxNewChangedate) {
		this.sxNewChangedate = sxNewChangedate;
	}

	public Integer getSxId() {
		return sxId;
	}

	public void setSxId(Integer sxId) {
		this.sxId = sxId;
	}

	public String getSxTitle() {
		return sxTitle;
	}

	public void setSxTitle(String sxTitle) {
		this.sxTitle = sxTitle;
	}

	public Date getSxChangedate() {
		return sxChangedate;
	}

	public void setSxChangedate(Date sxChangedate) {
		this.sxChangedate = sxChangedate;
	}

	public String getSxProcess() {
		return sxProcess;
	}

	public void setSxProcess(String sxProcess) {
		this.sxProcess = sxProcess;
	}

	public String getSxStatus() {
		return sxStatus;
	}

	public void setSxStatus(String sxStatus) {
		this.sxStatus = sxStatus;
	}

	public String getSxOriginal() {
		return sxOriginal;
	}

	public void setSxOriginal(String sxOriginal) {
		this.sxOriginal = sxOriginal;
	}

	public String getSxLb1() {
		return sxLb1;
	}

	public void setSxLb1(String sxLb1) {
		this.sxLb1 = sxLb1;
	}

	public String getSxLb2() {
		return sxLb2;
	}

	public void setSxLb2(String sxLb2) {
		this.sxLb2 = sxLb2;
	}

	public String getSxLb3() {
		return sxLb3;
	}

	public void setSxLb3(String sxLb3) {
		this.sxLb3 = sxLb3;
	}

	public String getSxProvince() {
		return sxProvince;
	}

	public void setSxProvince(String sxProvince) {
		this.sxProvince = sxProvince;
	}

	public String getSxCity() {
		return sxCity;
	}

	public void setSxCity(String sxCity) {
		this.sxCity = sxCity;
	}

	public String getSxCountry() {
		return sxCountry;
	}

	public void setSxCountry(String sxCountry) {
		this.sxCountry = sxCountry;
	}

	public String getSxPici() {
		return sxPici;
	}

	public void setSxPici(String sxPici) {
		this.sxPici = sxPici;
	}

	public String getSxShenpijc() {
		return sxShenpijc;
	}

	public void setSxShenpijc(String sxShenpijc) {
		this.sxShenpijc = sxShenpijc;
	}

	public String getSxLevels() {
		return sxLevels;
	}

	public void setSxLevels(String sxLevels) {
		this.sxLevels = sxLevels;
	}

	public String getSxDetails() {
		return sxDetails;
	}

	public void setSxDetails(String sxDetails) {
		this.sxDetails = sxDetails;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getSxShenpi() {
		return sxShenpi;
	}

	public void setSxShenpi(String sxShenpi) {
		this.sxShenpi = sxShenpi;
	}

    public void setJt_ck(Integer jt_ck) {
        this.jt_ck = jt_ck;
    }

    public Integer getJt_ck() {
        return jt_ck;
    }
}
