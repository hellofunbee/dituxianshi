package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.support.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LvyouMapper;
import com.ruoyi.system.domain.Lvyou;
import com.ruoyi.system.service.ILvyouService;

/**
 * 旅游 服务层实现
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Service
public class LvyouServiceImpl implements ILvyouService 
{
	@Autowired
	private LvyouMapper lvyouMapper;

	/**
     * 查询旅游信息
     * 
     * @param lvId 旅游ID
     * @return 旅游信息
     */
    @Override
	public Lvyou selectLvyouById(Integer lvId)
	{
	    return lvyouMapper.selectLvyouById(lvId);
	}
	
	/**
     * 查询旅游列表
     * 
     * @param lvyou 旅游信息
     * @return 旅游集合
     */
	@Override
	public List<Lvyou> selectLvyouList(Lvyou lvyou)
	{
	    return lvyouMapper.selectLvyouList(lvyou);
	}
	
    /**
     * 新增旅游
     * 
     * @param lvyou 旅游信息
     * @return 结果
     */
	@Override
	public int insertLvyou(Lvyou lvyou)
	{
	    return lvyouMapper.insertLvyou(lvyou);
	}
	
	/**
     * 修改旅游
     * 
     * @param lvyou 旅游信息
     * @return 结果
     */
	@Override
	public int updateLvyou(Lvyou lvyou)
	{
	    return lvyouMapper.updateLvyou(lvyou);
	}

	/**
	 * 图面化交通
	 *
	 * @param lvyou 交通信息
	 * @return 结果
	 */
	@Override
	public int updateLvyouRoleName(Lvyou lvyou)
	{
		return lvyouMapper.updateLvyouRoleName(lvyou);
	}

	/**
     * 删除旅游对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteLvyouByIds(String ids)
	{
		return lvyouMapper.deleteLvyouByIds(Convert.toStrArray(ids));
	}

	/**
	 * 图面化交通
	 *
	 * @param lvyou 交通信息
	 * @return 结果
	 */
	@Override
	public int updateLvyouImg(Lvyou lvyou)
	{
		return lvyouMapper.updateLvyouImg(lvyou);
	}

	/**
	 * 审批交通
	 *
	 * @param lvyou 交通信息
	 * @return 结果
	 */
	@Override
	public int updateLvyouShenpi(Lvyou lvyou)
	{
		return lvyouMapper.updateLvyouShenpi(lvyou);
	}
}
