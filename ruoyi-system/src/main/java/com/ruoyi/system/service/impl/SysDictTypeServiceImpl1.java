package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.system.domain.SysDictType1;
import com.ruoyi.system.service.ISysDictTypeService1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.support.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.mapper.SysDictDataMapper1;
import com.ruoyi.system.mapper.SysDictTypeMapper1;

/**
 * 字典 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysDictTypeServiceImpl1 implements ISysDictTypeService1
{
    @Autowired
    private SysDictTypeMapper1 dictTypeMapper1;

    @Autowired
    private SysDictDataMapper1 dictDataMapper1;

    /**
     * 根据条件分页查询字典类型
     * 
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    @Override
    public List<SysDictType1> selectDictTypeList(SysDictType1 dictType)
    {
        return dictTypeMapper1.selectDictTypeList(dictType);
    }

    /**
     * 查询字典管理树
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    //@Override
    @DataScope(tableAlias = "d")
    public List<Map<String, Object>> selectDictTypeTree(SysDictType1 dictType)
    {
        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        List<SysDictType1> dictTypeList = dictTypeMapper1.selectDictTypeList(dictType);
        trees = getTrees(dictTypeList, false, null);
        return trees;
    }

    /**
     * 对象转字典树
     *
     * @param dictTypeList 部门列表
     * @param isCheck 是否需要选中
     * @param roleDictTypeList 角色已存在菜单列表
     * @return
     */
    public List<Map<String, Object>> getTrees(List<SysDictType1> dictTypeList, boolean isCheck, List<String> roleDictTypeList)
    {

        List<Map<String, Object>> trees = new ArrayList<Map<String, Object>>();
        for (SysDictType1 dictType : dictTypeList)
        {
            if (UserConstants.DEPT_NORMAL.equals(dictType.getStatus()))
            {
                Map<String, Object> dictTypeMap = new HashMap<String, Object>();
                dictTypeMap.put("id", dictType.getDictId());
                dictTypeMap.put("pId", dictType.getDt_parentId());
                dictTypeMap.put("name", dictType.getDictName());
                dictTypeMap.put("title", dictType.getDictName());
                if (isCheck)
                {
                    dictTypeMap.put("checked", roleDictTypeList.contains(dictType.getDictId() + dictType.getDictName()));
                }
                else
                {
                    dictTypeMap.put("checked", false);
                }
                trees.add(dictTypeMap);
            }
        }
        return trees;
    }

    /**
     * 根据所有字典类型
     * 
     * @return 字典类型集合信息
     */
    @Override
    public List<SysDictType1> selectDictTypeAll()
    {
        return dictTypeMapper1.selectDictTypeAll();
    }

    /**
     * 根据字典类型ID查询信息
     * 
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    @Override
    public SysDictType1 selectDictTypeById(Long dictId)
    {
        return dictTypeMapper1.selectDictTypeById(dictId);
    }

    /**
     * 通过字典ID删除字典信息
     * 
     * @param dictId 字典ID
     * @return 结果
     */
    @Override
    public int deleteDictTypeById(Long dictId)
    {
        return dictTypeMapper1.deleteDictTypeById(dictId);
    }

    /**
     * 批量删除字典类型
     * 
     * @param ids 需要删除的数据
     * @return 结果
     */
    @Override
    public int deleteDictTypeByIds(String ids) throws BusinessException
    {
        Long[] dictIds = Convert.toLongArray(ids);
        for (Long dictId : dictIds)
        {
            SysDictType1 dictType = selectDictTypeById(dictId);
            if (dictDataMapper1.countDictDataByType(dictType.getDictType()) > 0)
            {
                throw new BusinessException(String.format("%1$s已分配,不能删除", dictType.getDictName()));
            }
        }

        return dictTypeMapper1.deleteDictTypeByIds(dictIds);
    }

    /**
     * 新增保存字典类型信息
     * 
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Override
    public int insertDictType(SysDictType1 dictType)
    {
        return dictTypeMapper1.insertDictType(dictType);
    }

    /**
     * 修改保存字典类型信息
     * 
     * @param dictType 字典类型信息
     * @return 结果
     */
    @Override
    public int updateDictType(SysDictType1 dictType)
    {
        SysDictType1 oldDict = dictTypeMapper1.selectDictTypeById(dictType.getDictId());
        dictDataMapper1.updateDictDataType(oldDict.getDictType(), dictType.getDictType());
        return dictTypeMapper1.updateDictType(dictType);
    }

    /**
     * 校验字典类型称是否唯一
     * 
     * @param dict 字典类型
     * @return 结果
     */
    @Override
    public String checkDictTypeUnique(SysDictType1 dict)
    {
        Long dictId = StringUtils.isNull(dict.getDictId()) ? -1L : dict.getDictId();
        SysDictType1 dictType = dictTypeMapper1.checkDictTypeUnique(dict.getDictType());
        if (StringUtils.isNotNull(dictType) && dictType.getDictId().longValue() != dictId.longValue())
        {
            return UserConstants.DICT_TYPE_NOT_UNIQUE;
        }
        return UserConstants.DICT_TYPE_UNIQUE;
    }
}
