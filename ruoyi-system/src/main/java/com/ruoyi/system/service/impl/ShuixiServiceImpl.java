package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.support.Convert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ShuixiMapper;
import com.ruoyi.system.domain.Shuixi;
import com.ruoyi.system.service.IShuixiService;

/**
 * 水系 服务层实现
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Service
public class ShuixiServiceImpl implements IShuixiService 
{
	@Autowired
	private ShuixiMapper shuixiMapper;

	/**
     * 查询水系信息
     * 
     * @param sxId 水系ID
     * @return 水系信息
     */
    @Override
	public Shuixi selectShuixiById(Integer sxId)
	{
	    return shuixiMapper.selectShuixiById(sxId);
	}
	
	/**
     * 查询水系列表
     * 
     * @param shuixi 水系信息
     * @return 水系集合
     */
	@Override
	public List<Shuixi> selectShuixiList(Shuixi shuixi)
	{
	    return shuixiMapper.selectShuixiList(shuixi);
	}
	
    /**
     * 新增水系
     * 
     * @param shuixi 水系信息
     * @return 结果
     */
	@Override
	public int insertShuixi(Shuixi shuixi)
	{
	    return shuixiMapper.insertShuixi(shuixi);
	}
	
	/**
     * 修改水系
     * 
     * @param shuixi 水系信息
     * @return 结果
     */
	@Override
	public int updateShuixi(Shuixi shuixi)
	{
	    return shuixiMapper.updateShuixi(shuixi);
	}

	/**
	 * 修改水系
	 *
	 * @param shuixi 水系信息
	 * @return 结果
	 */
	@Override
	public int updateShuixiRoleName(Shuixi shuixi)
	{
		return shuixiMapper.updateShuixiRoleName(shuixi);
	}

	/**
     * 删除水系对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteShuixiByIds(String ids)
	{
		return shuixiMapper.deleteShuixiByIds(Convert.toStrArray(ids));
	}

	/**
	 * 图面化交通
	 *
	 * @param shuixi 交通信息
	 * @return 结果
	 */
	@Override
	public int updateShuixiImg(Shuixi shuixi)
	{
		return shuixiMapper.updateShuixiImg(shuixi);
	}

	/**
	 * 审批交通
	 *
	 * @param shuixi 交通信息
	 * @return 结果
	 */
	@Override
	public int updateShuixiShenpi(Shuixi shuixi)
	{
		return shuixiMapper.updateShuixiShenpi(shuixi);
	}
	
}
