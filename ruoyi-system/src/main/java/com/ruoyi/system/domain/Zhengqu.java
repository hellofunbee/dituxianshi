package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.base.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;

/**
 * 区划地名表 zhengqu
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public class Zhengqu extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 区划地名 */
	@Excel(name = "政区序号")
	private Integer zqId;
	/** 标题 */
	@Excel(name = "标题")
	private String zqTitle;
	/** 变更日期 */
	@Excel(name = "变更日期",dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date zqChangedate;
	/** 信息来源 */
	@Excel(name = "信息来源")
	private String zqOriginal;

	@Excel(name = "一级类别")
	private String class1;
	@Excel(name = "二级类别")
	private String class2;
	@Excel(name = "三级类别")
	private String class3;
	@Excel(name = "所在省")
	private String sheng;
	@Excel(name = "所在市")
	private String shi;
	@Excel(name = "所在县")
	private String xian;
	private SysCategory category;
	private SysArea area;

	/** 一级类别 */
	private String zqLb1;
	/** 二级类别 */
	private String zqLb2;
	/** 三级类别 */
	private String zqLb3;
	/** 所在省 */
	private String zqProvince;
	/** 所在市 */
	private String zqCity;
	/** 所在县 */
	private String zqCountry;
	/** 原名称 */
	@Excel(name = "原名称")
	private String zqOldname;
	/** 新名称 */
	@Excel(name = "新名称")
	private String zqNewname;
	/** 详细内容 */
	@Excel(name = "详细内容")
	private String zqDetails;
	/** 图片保存路径 */
	private String avatar;
	/** 审批内容 */
	private String zqShenpi;
	/** 新变更日期 */
	private String zqNewChangedate;
	/** 登录角色 */
	private String zqRoleName;

	/** 地图数据库 */
	@Excel(name = "图片上传情况")
	private String zqProcess;
	/** 审批状态 */
	@Excel(name = "审批状态")
	private String zqStatus;

	/** 地图院数据库审批 */
	@Excel(name = "地图院数据库审批状态", readConverterExp = "-1=未选中,0=未选中,1=请修改,2=已修改,3=审核通过")
	private Integer jt_ck;


	public String getClass1() {
		return class1;
	}

	public void setClass1(String class1) {
		this.class1 = class1;
	}

	public String getClass2() {
		return class2;
	}

	public void setClass2(String class2) {
		this.class2 = class2;
	}

	public String getClass3() {
		return class3;
	}

	public void setClass3(String class3) {
		this.class3 = class3;
	}

	public String getSheng() {
		return sheng;
	}

	public void setSheng(String sheng) {
		this.sheng = sheng;
	}

	public String getShi() {
		return shi;
	}

	public void setShi(String shi) {
		this.shi = shi;
	}

	public String getXian() {
		return xian;
	}

	public void setXian(String xian) {
		this.xian = xian;
	}

	public SysCategory getCategory() {
		return category;
	}

	public void setCategory(SysCategory category) {
		this.category = category;
	}

	public SysArea getArea() {
		return area;
	}

	public void setArea(SysArea area) {
		this.area = area;
	}

	public String getZqRoleName() {
		return zqRoleName;
	}

	public void setZqRoleName(String zqRoleName) {
		this.zqRoleName = zqRoleName;
	}

	public String getZqNewChangedate() {
		return zqNewChangedate;
	}

	public void setZqNewChangedate(String zqNewChangedate) {
		this.zqNewChangedate = zqNewChangedate;
	}

	public Integer getZqId() {
		return zqId;
	}

	public void setZqId(Integer zqId) {
		this.zqId = zqId;
	}

	public String getZqTitle() {
		return zqTitle;
	}

	public void setZqTitle(String zqTitle) {
		this.zqTitle = zqTitle;
	}

	public Date getZqChangedate() {
		return zqChangedate;
	}

	public void setZqChangedate(Date zqChangedate) {
		this.zqChangedate = zqChangedate;
	}

	public String getZqProcess() {
		return zqProcess;
	}

	public void setZqProcess(String zqProcess) {
		this.zqProcess = zqProcess;
	}

	public String getZqStatus() {
		return zqStatus;
	}

	public void setZqStatus(String zqStatus) {
		this.zqStatus = zqStatus;
	}

	public String getZqOriginal() {
		return zqOriginal;
	}

	public void setZqOriginal(String zqOriginal) {
		this.zqOriginal = zqOriginal;
	}

	public String getZqLb1() {
		return zqLb1;
	}

	public void setZqLb1(String zqLb1) {
		this.zqLb1 = zqLb1;
	}

	public String getZqLb2() {
		return zqLb2;
	}

	public void setZqLb2(String zqLb2) {
		this.zqLb2 = zqLb2;
	}

	public String getZqLb3() {
		return zqLb3;
	}

	public void setZqLb3(String zqLb3) {
		this.zqLb3 = zqLb3;
	}

	public String getZqProvince() {
		return zqProvince;
	}

	public void setZqProvince(String zqProvince) {
		this.zqProvince = zqProvince;
	}

	public String getZqCity() {
		return zqCity;
	}

	public void setZqCity(String zqCity) {
		this.zqCity = zqCity;
	}

	public String getZqCountry() {
		return zqCountry;
	}

	public void setZqCountry(String zqCountry) {
		this.zqCountry = zqCountry;
	}

	public String getZqOldname() {
		return zqOldname;
	}

	public void setZqOldname(String zqOldname) {
		this.zqOldname = zqOldname;
	}

	public String getZqNewname() {
		return zqNewname;
	}

	public void setZqNewname(String zqNewname) {
		this.zqNewname = zqNewname;
	}

	public String getZqDetails() {
		return zqDetails;
	}

	public void setZqDetails(String zqDetails) {
		this.zqDetails = zqDetails;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getZqShenpi() {
		return zqShenpi;
	}

	public void setZqShenpi(String zqShenpi) {
		this.zqShenpi = zqShenpi;
	}

    public void setJt_ck(Integer jt_ck) {
        this.jt_ck = jt_ck;
    }

    public Integer getJt_ck() {
        return jt_ck;
    }
}
