package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.Jiaotong;
import com.ruoyi.system.domain.Shuixi;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.service.IShuixiService;
import com.ruoyi.system.service.ISysCategoryService;
import com.ruoyi.system.util.MyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 水系 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Controller
@RequestMapping("/system/shuixi")
public class ShuixiController extends BaseController
{
	private static final Logger log = LoggerFactory.getLogger(SysProfileController.class);
    private String prefix = "system/shuixi";
	@Autowired
	private MyWebScoketController websc;
	@Autowired
	private ISysCategoryService categoryService;
	@Autowired
	private IShuixiService shuixiService;
	
	@RequiresPermissions("system:shuixi:view")
	@GetMapping()
	public String shuixi()
	{
	    return prefix + "/shuixi";
	}
	
	/**
	 * 查询水系列表
	 */
	@RequiresPermissions("system:shuixi:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Shuixi shuixi)
	{
		startPage();
		shuixi.setSxRoleName(ShiroUtils.getSysUser().getUserName());
		shuixiService.updateShuixiRoleName(shuixi);
		shuixi.setSxTitle(MyUtil.getSql(shuixi.getSxTitle(),"sx_title"));
		shuixi.setSxDetails(MyUtil.getSql(shuixi.getSxDetails(),"sx_details"));
        List<Shuixi> list = shuixiService.selectShuixiList(shuixi);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出水系列表
	 */
	@RequiresPermissions("system:shuixi:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Shuixi shuixi)
    {
		shuixi.setSxTitle(MyUtil.getSql(shuixi.getSxTitle(),"sx_title"));
		shuixi.setSxDetails(MyUtil.getSql(shuixi.getSxDetails(),"sx_details"));

		List<Shuixi> list = shuixiService.selectShuixiList(shuixi);
        ExcelUtil<Shuixi> util = new ExcelUtil<Shuixi>(Shuixi.class);
        return util.exportExcel(list, "水系信息");
    }
	
	/**
	 * 新增水系
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存水系
	 */
	@RequiresPermissions("system:shuixi:add")
	@Log(title = "水系", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Shuixi shuixi)
	{
		shuixi.setCreateBy(ShiroUtils.getLoginName());
		return toAjax(shuixiService.insertShuixi(shuixi));
	}

	/**
	 * 修改水系
	 */
	@GetMapping("/edit/{sxId}")
	public String edit(@PathVariable("sxId") Integer sxId, ModelMap mmap)
	{
		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(shuixi.getSxChangedate()!=null) {
			String changedateString = formatter.format(shuixi.getSxChangedate());
			shuixi.setSxNewChangedate(changedateString);
		}
		mmap.put("shuixi", shuixi);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存水系
	 */
	@RequiresPermissions("system:shuixi:edit")
	@Log(title = "水系", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Shuixi shuixi)
	{
		shuixi.setUpdateBy(ShiroUtils.getLoginName());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date currentTime_2  = formatter.parse(shuixi.getSxNewChangedate(),pos);
		shuixi.setSxChangedate(currentTime_2);
		return toAjax(shuixiService.updateShuixi(shuixi));
	}
	
	/**
	 * 删除水系
	 */
	@RequiresPermissions("system:shuixi:remove")
	@Log(title = "水系", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(shuixiService.deleteShuixiByIds(ids));
	}

	/**
	 * 预览水系
	 */
	@GetMapping("/info/{sxId}")
	public String info(@PathVariable("sxId") Integer sxId, ModelMap mmap)
	{
		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(shuixi.getCreateTime());
		if(shuixi.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(shuixi.getUpdateTime());
			shuixi.setNewupdateTime(updatedateString);
		}
		if(shuixi.getSxChangedate()!=null) {
			String changedateString = formatter.format(shuixi.getSxChangedate());
			shuixi.setSxNewChangedate(changedateString);
		}
		shuixi.setNewcreateTime(createdateString);

		mmap.put("shuixi", shuixi);
		return prefix + "/info";
	}

	/**
	 * 预览水系
	 */
	@GetMapping("/infoFront/{sxId}")
	public String infoFront(@PathVariable("sxId") Integer sxId, ModelMap mmap, SysCategory category, String title)
	{

		SysCategory categoryNew = new SysCategory();

		if (category == null) {
			category = new SysCategory();
		}

		//所有的节点
		SysCategory c = new SysCategory();
		c.setParentId(100L);
		List<SysCategory> categoryList = categoryService.selectDeptList(c);

		//子目录
		List<SysCategory> childs = new ArrayList<>();

		if (category.getLevel() == null || category.getLevel() == 1) {
			if (category.getDeptId() == null) {
				categoryNew.setParentId(categoryList.get(0).getDeptId());
			} else {
				for (SysCategory n : categoryList) {
					if (n.getDeptId() == category.getDeptId()) {
						n.setCurr(true);
						categoryNew.setParentId(n.getDeptId());
					}
				}
			}
			childs = categoryService.selectDeptList(categoryNew);

		} else if (category.getLevel() == 2) {
			category = categoryService.selectDeptById(category.getDeptId());
			category.setCurr(true);
			categoryNew.setParentId(category.getParentId());
			//选中父级
			for (SysCategory n : categoryList) {
				if (n.getDeptId() == category.getParentId()) {
					n.setCurr(true);
				}
			}
			childs = categoryService.selectDeptList(categoryNew);
			//选中子级
			for (SysCategory c1 : childs) {
				if (c1.getDeptId() == category.getDeptId()) {
					System.out.println(c1.getDeptId() + "=="+ category.getDeptId());
					c1.setCurr(true);
				}
			}
		}
		mmap.put("nodes", categoryList);
		mmap.put("title", title);
		mmap.put("curr", categoryNew);
		mmap.put("curr_child", category);
		mmap.put("childs", childs);


		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(shuixi.getCreateTime());
		if(shuixi.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(shuixi.getUpdateTime());
			shuixi.setNewupdateTime(updatedateString);
		}
		if(shuixi.getSxChangedate()!=null) {
			String changedateString = formatter.format(shuixi.getSxChangedate());
			shuixi.setSxNewChangedate(changedateString);
		}
		shuixi.setNewcreateTime(createdateString);

		mmap.put("shuixi", shuixi);
		return prefix + "/infoFront";
	}

	/**
	 * 图面化
	 */
	@GetMapping("/avatar/{sxId}")
	public String avatar(@PathVariable("sxId") Integer sxId, ModelMap mmap) {
		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		mmap.put("shuixi", shuixi);
		return prefix + "/avatar";
	}
	/**
	 * 保存图像
	 */
	@Log(title = "图面化信息", businessType = BusinessType.UPDATE)
	@PostMapping("/updateAvatar")
	@ResponseBody
	public AjaxResult updateAvatar(MultipartFile file, Shuixi sx) {
		try {
			sx.setEditorBy(ShiroUtils.getLoginName());
			return toAjax(shuixiService.updateShuixiImg(sx));

           /* if (file != null && !file.isEmpty()) {
//				if (shuixiService.updateShuixi(shuixi) > 0)
//				{
//					setSysUser(shuixiService.selectShuixiById(shuixi.getUserId()));
//					return success();
//				}
            } else {
                return error();
            }*/
		} catch (Exception e) {
			log.error("修改头像失败！", e);
			return error(e.getMessage());
		}
	}

	/**
	 * 审批交通
	 */
	@GetMapping("/shenpi/{sxId}")
	public String shenpi(@PathVariable("sxId") Integer sxId, ModelMap mmap) {
		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		mmap.put("shuixi", shuixi);
		return prefix + "/shenpi";
	}

	/**
	 * 审批保存交通
	 */
	@Log(title = "交通", businessType = BusinessType.UPDATE)
	@PostMapping("/shenpiSave")
	@ResponseBody
	public AjaxResult shenpiSave(Shuixi shuixi) throws IOException {
		SysNotice notice = new SysNotice();
		notice.setNoticeType(shuixi.getSxStatus());
		Shuixi jt = shuixiService.selectShuixiById(shuixi.getSxId());
		notice.setNoticeTitle("标题为" + "【" + jt.getSxTitle() + "】" + "的地图现势资料信息"+ "：审批通过" );
		websc.sendInfo(JSONObject.toJSONString(notice));
		return toAjax(shuixiService.updateShuixiShenpi(shuixi));
	}

	/**
	 * 预览审批交通
	 */
	@GetMapping("/shenpiinfo/{sxId}")
	public String shenpiinfo(@PathVariable("sxId") Integer sxId, ModelMap mmap) {
		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		mmap.put("shuixi", shuixi);
		return prefix + "/shenpiinfo";
	}

	/**
	 * 预览图面化交通
	 */
	@GetMapping("/avatarinfo/{sxId}")
	public String avatarinfo(@PathVariable("sxId") Integer sxId, ModelMap mmap) {
		Shuixi shuixi = shuixiService.selectShuixiById(sxId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String editordateString = formatter.format(shuixi.getEditorTime());
		shuixi.setNeweditorTime(editordateString);
		mmap.put("shuixi", shuixi);
		return prefix + "/avatarinfo";
	}
}
