package com.ruoyi.system.util.xml;

import com.ruoyi.common.utils.DateUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class XMLUtil {
    /**
     * 类转xml方法.
     *
     * @return String
     * @throws
     */
    public static String list2xml(List<?> list) {
        Document document = DocumentHelper.createDocument();
        Element nodesElement = document.addElement("DATA");
        list2xml(list, nodesElement);
        return doc2String(document);
    }

    /**
     * xml转为String.
     *
     * @param document
     * @return
     * @throws
     */
    public static String doc2String(Document document) {
        String s = "";
        try {
            // 使用输出流来进行转化
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            // 使用UTF-8编码
            OutputFormat format = new OutputFormat("   ", true, "UTF-8");
            XMLWriter writer = new XMLWriter(out, format);
            writer.write(document);
            s = out.toString("UTF-8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return s;
    }

    /**
     * List2XML,目前支持List<List> List<Map> List<Map<String,List>>等只有
     * List Map 组合的数据进行转换.
     *
     * @param list
     * @param element
     * @return
     * @throws
     */
    public static Element list2xml(List list, Element element) {
        int i = 0;
        for (Object o : list) {
            Element nodeElement = element.addElement("LIST");
            if (o instanceof Map) {
                nodeElement.addAttribute("type", "o");
                Map m = (Map) o;
                for (Iterator iterator = m.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry entry = (Map.Entry) iterator.next();
                    Element keyElement = nodeElement.addElement(entry.getKey().toString());
                    if (entry.getValue() instanceof List) {
                        keyElement.addAttribute("type", "l");
                        list2xml((List) entry.getValue(), keyElement);
                    } else {
                        keyElement.addAttribute("type", "s");
                        keyElement.setText(String.valueOf(entry.getValue()));
                    }
                }
            } else if (o instanceof List) {
                nodeElement.addAttribute("type", "l");
                list2xml((List) o, nodeElement);
            } else {
                Element keyElement = nodeElement.addElement("value");
                keyElement.addAttribute("num", String.valueOf(i));
                keyElement.setText(String.valueOf(o));
            }
            i++;
        }
        return element;
    }

    /**
     * xml转List方法.
     *
     * @param xml
     * @return List<?>
     * @throws
     */
    public static List<?> xml2List(String xml) {
        try {
            List list = new ArrayList();
            Document document = DocumentHelper.parseText(xml);
            Element nodesElement = document.getRootElement();
            List nodes = nodesElement.elements();
            for (Iterator its = nodes.iterator(); its.hasNext(); ) {
                Element nodeElement = (Element) its.next();
                if (("l").equals(nodeElement.attributeValue("type"))) {
                    List s = xml2List(nodeElement.asXML());
                    list.add(s);
                    s = null;
                } else if (("o").equals(nodeElement.attributeValue("type"))) {
                    Map map = xml2Map(nodeElement.asXML());
                    list.add(map);
                    map = null;
                } else {
                    list.add(nodeElement.getText());
                }
            }
            nodes = null;
            nodesElement = null;
            document = null;
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * xml 2 map
     *
     * @param xml
     * @return
     */
    public static Map xml2Map(String xml) {
        try {
            Map map = new HashMap();
            Document document = DocumentHelper.parseText(xml);
            Element nodeElement = document.getRootElement();
            List node = nodeElement.elements();
            for (Iterator it = node.iterator(); it.hasNext(); ) {
                Element elm = (Element) it.next();
                if ("l".equals(elm.attributeValue("type"))) {
                    map.put(elm.getName(), xml2List(elm.asXML()));
                } else if ("o".equals(elm.attributeValue("type"))) {
                    map.put(elm.getName(), xml2Map(elm.asXML()));
                } else {
                    map.put(elm.getName(), elm.getText());
                }
                elm = null;
            }
            node = null;
            nodeElement = null;
            document = null;
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {

       String s =  DateUtils.getTime();

        System.out.println(s);
    }


}