package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.SysArea;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.service.ISysAreaService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 分类信息
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/area")
public class SysAreaController extends BaseController {
    private String prefix = "system/area";

    @Autowired
    private ISysAreaService deptService;

    @RequiresPermissions("system:area:view")
    @GetMapping()
    public String dept() {
        return prefix + "/dept";
    }

    @RequiresPermissions("system:area:view")
    @GetMapping("/test")
    public String test() {
        return prefix + "/test";
    }

    @RequiresPermissions("system:area:list")
    @GetMapping("/list")
    @ResponseBody
    public List<SysArea> list(SysArea dept) {
        List<SysArea> deptList = deptService.selectDeptList(dept);
        return deptList;
    }

    /**
     * 新增分类
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap) {
        mmap.put("dept", deptService.selectDeptById(parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存分类
     */
    @Log(title = "分类管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:area:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysArea dept) {
        dept.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(deptService.insertDept(dept));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{deptId}")
    public String edit(@PathVariable("deptId") Long deptId, ModelMap mmap) {
        SysArea dept = deptService.selectDeptById(deptId);
        if (StringUtils.isNotNull(dept) && 1L == deptId) {
            dept.setParentName("无");
        }
        mmap.put("dept", dept);
        return prefix + "/edit";
    }

    /**
     * 保存
     */
    @Log(title = "分类管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:area:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysArea dept) {
        dept.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(deptService.updateDept(dept));
    }

    /**
     * 删除
     */
    @Log(title = "分类管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:area:remove")
    @PostMapping("/remove/{deptId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("deptId") Long deptId) {
        if (deptService.selectDeptCount(deptId) > 0) {
            return error(1, "存在下级分类,不允许删除");
        }
        if (deptService.checkDeptExistUser(deptId)) {
            return error(1, "分类存在用户,不允许删除");
        }
        return toAjax(deptService.deleteDeptById(deptId));
    }

    /**
     * 校验分类名称
     */
    @PostMapping("/checkDeptNameUnique")
    @ResponseBody
    public String checkDeptNameUnique(SysArea dept) {
        return deptService.checkDeptNameUnique(dept);
    }

    /**
     * 选择分类树
     */
    @GetMapping("/selectDeptTree/{deptId}")
    public String selectDeptTree(@PathVariable("deptId") Long deptId, ModelMap mmap) {
        mmap.put("dept", deptService.selectDeptById(deptId));
        return prefix + "/tree";
    }

    /**
     * 加载分类列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Map<String, Object>> treeData() {
        List<Map<String, Object>> tree = deptService.selectDeptTree(new SysArea());
        return tree;
    }

    /**
     * 加载角色分类（数据权限）列表树
     */
    @GetMapping("/roleDeptTreeData")
    @ResponseBody
    public List<Map<String, Object>> deptTreeData(SysRole role) {
        List<Map<String, Object>> tree = deptService.roleDeptTreeData(role);
        return tree;
    }
}
