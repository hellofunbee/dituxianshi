package com.ruoyi.system.util;

import java.util.Map;

public interface Configurable {
	public String getPrefix();

	public Map<String, String> getCustoms();
}
