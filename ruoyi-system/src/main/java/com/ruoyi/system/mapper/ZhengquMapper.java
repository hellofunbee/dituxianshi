package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Zhengqu;
import java.util.List;	

/**
 * 区划地名 数据层
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public interface ZhengquMapper 
{
	/**
     * 查询区划地名信息
     * 
     * @param zqId 区划地名ID
     * @return 区划地名信息
     */
	public Zhengqu selectZhengquById(Integer zqId);
	
	/**
     * 查询区划地名列表
     * 
     * @param zhengqu 区划地名信息
     * @return 区划地名集合
     */
	public List<Zhengqu> selectZhengquList(Zhengqu zhengqu);
	
	/**
     * 新增区划地名
     * 
     * @param zhengqu 区划地名信息
     * @return 结果
     */
	public int insertZhengqu(Zhengqu zhengqu);
	
	/**
     * 修改区划地名
     * 
     * @param zhengqu 区划地名信息
     * @return 结果
     */
	public int updateZhengqu(Zhengqu zhengqu);

	/**
	 * 修改区划地名
	 *
	 * @param zhengqu 区划地名信息
	 * @return 结果
	 */
	public int updateZhengquRoleName(Zhengqu zhengqu);

	/**
     * 删除区划地名
     * 
     * @param zqId 区划地名ID
     * @return 结果
     */
	public int deleteZhengquById(Integer zqId);
	
	/**
     * 批量删除区划地名
     * 
     * @param zqIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteZhengquByIds(String[] zqIds);

	/**
	 * 图面化交通
	 *
	 * @param zhengqu 交通信息
	 * @return 结果
	 */
	public int updateZhengquImg(Zhengqu zhengqu);

	/**
	 * 审批交通
	 *
	 * @param zhengqu 交通信息
	 * @return 结果
	 */
	public int updateZhengquShenpi(Zhengqu zhengqu);
}