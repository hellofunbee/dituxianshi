package com.ruoyi.web.controller.system;

import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.Jiaotong;
import com.ruoyi.system.service.IJiaotongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 交通 信息操作处理
 *
 * @author ruoyi
 * @date 2019-03-21
 */
@Controller
@RequestMapping("/api/jiaotong")
public class JiaotongApiController extends BaseController {
    @Autowired
    private IJiaotongService jiaotongService;

    /**
     * 新增保存交通
     */
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Jiaotong jiaotong) {
        jiaotong.setCreateBy("admin");
        return toAjax(jiaotongService.insertJiaotong(jiaotong));
    }

}
