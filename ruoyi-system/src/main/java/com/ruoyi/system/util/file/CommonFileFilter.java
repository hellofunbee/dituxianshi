package com.ruoyi.system.util.file;

public interface CommonFileFilter {
	boolean accept(CommonFile file);
}
