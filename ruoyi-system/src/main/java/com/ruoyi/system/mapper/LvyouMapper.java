package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Lvyou;
import java.util.List;	

/**
 * 旅游 数据层
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public interface LvyouMapper 
{
	/**
     * 查询旅游信息
     * 
     * @param lvId 旅游ID
     * @return 旅游信息
     */
	public Lvyou selectLvyouById(Integer lvId);
	
	/**
     * 查询旅游列表
     * 
     * @param lvyou 旅游信息
     * @return 旅游集合
     */
	public List<Lvyou> selectLvyouList(Lvyou lvyou);
	
	/**
     * 新增旅游
     * 
     * @param lvyou 旅游信息
     * @return 结果
     */
	public int insertLvyou(Lvyou lvyou);
	
	/**
     * 修改旅游
     * 
     * @param lvyou 旅游信息
     * @return 结果
     */
	public int updateLvyou(Lvyou lvyou);


	/**
	 * 图面化交通
	 *
	 * @param lvyou 交通信息
	 * @return 结果
	 */
	public int updateLvyouRoleName(Lvyou lvyou);
	/**
     * 删除旅游
     * 
     * @param lvId 旅游ID
     * @return 结果
     */
	public int deleteLvyouById(Integer lvId);
	
	/**
     * 批量删除旅游
     * 
     * @param lvIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteLvyouByIds(String[] lvIds);

	/**
	 * 图面化交通
	 *
	 * @param lvyou 交通信息
	 * @return 结果
	 */
	public int updateLvyouImg(Lvyou lvyou);

	/**
	 * 审批交通
	 *
	 * @param lvyou 交通信息
	 * @return 结果
	 */
	public int updateLvyouShenpi(Lvyou lvyou);
}