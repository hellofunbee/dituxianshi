package com.ruoyi.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

/**
 * 生成一个Base64唯一字符串
 *
 * @author guobin
 * @date 2020-12-06
 */
public class ShiroAESEncryption {
    private final static Logger logger = LoggerFactory.getLogger(ShiroAESEncryption.class);

        /**
         * 随机生成秘钥，参考org.apache.shiro.crypto.AbstractSymmetricCipherService#generateNewKey(int)
         * @return
         */
        public static byte[] generateNewKey() {
            KeyGenerator kg;
            try {
                kg = KeyGenerator.getInstance("AES");
            } catch (NoSuchAlgorithmException var5) {
                String msg = "Unable to acquire AES algorithm.  This is required to function.";
                throw new IllegalStateException(msg, var5);
            }

            kg.init(128);
            SecretKey key = kg.generateKey();
            byte[] encoded = key.getEncoded();
            return encoded;
        }



    public static void main(String[] args) {
        generateNewKey();
    }
}