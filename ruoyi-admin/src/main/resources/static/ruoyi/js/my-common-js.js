var img_root = '/profile/upload/';
var dashuju = 'http://123.57.89.97:8088'

/**************************************时间格式化处理************************************/
function dateFtt(date, fmt) { //author: meizz

    try {
        if (!date) {
            return '--'
        }
        if (!fmt) {
            fmt = 'yyyy-MM-dd'
        }
        date = new Date(date);
        var o = {
            "M+": date.getMonth() + 1,     //月份
            "d+": date.getDate(),     //日
            "h+": date.getHours(),     //小时
            "m+": date.getMinutes(),     //分
            "s+": date.getSeconds(),     //秒
            "q+": Math.floor((date.getMonth() + 3) / 3), //季度
            "S": date.getMilliseconds()    //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    } catch (e) {
        console.log(e);
        return '--';
    }

}

/* 改变地图数据库的状态 type: 1:交通 2：旅游 3：区划地名 4：水系 5： 综合*/
function myChangeStatus(id, status, type, cllback) {
    $.modal.confirm("确认修改?", function () {

        $.ajax({
            url: ctx + "system/jiaotong/changeStatus",
            type: 'POST',
            dataType: "json",
            data: {"id": id, "status": status, type: type},
            beforeSend: function () {
                $.modal.loading("正在处理中，请稍后...");
            },
            success: function (data) {
                if (typeof cllback == "function") {
                    cllback(data);
                }
            }
        })
    }, function () {
        $.table.refresh()
    })
}

/**改变地图数据库的状态 点击事件*/
function mysetAction() {
    $("input[class=ck1]").click(function () {
        if ($(this).is(':checked')) {

            myChangeStatus($(this).attr('data-id'), 1, $(this).attr('data-type'), function (result) {
                $.operate.ajaxSuccess(result);
            });
        } else {

            myChangeStatus($(this).attr('data-id'), -1, $(this).attr('data-type'), function (result) {
                $.operate.ajaxSuccess(result);
            });

        }
    })
    $("input[class = ck2]").click(function () {
        // alert($(this).is(':checked'))
        if ($(this).is(':checked')) {
            myChangeStatus($(this).attr('data-id'), 2, $(this).attr('data-type'), function (result) {
                $.operate.ajaxSuccess(result);
            });
        } else {
            myChangeStatus($(this).attr('data-id'), 1, $(this).attr('data-type'), function (result) {
                $.operate.ajaxSuccess(result);
            });
        }
    });
    $("input[class = ck3]").click(function () {
        if ($(this).is(':checked')) {
            myChangeStatus($(this).attr('data-id'), 3, $(this).attr('data-type'), function (result) {
                $.operate.ajaxSuccess(result);
            });
        } else {
            myChangeStatus($(this).attr('data-id'), 2, $(this).attr('data-type'), function (result) {
                $.operate.ajaxSuccess(result);

            });
        }
    });
}


/**改变地图数据库的状态 设置按钮*/
function mySetBtns(value, row, index, field, type, qxg, yxg, ysh) {
    var actions = [];

    var attrs = [];
    var cs = [];
    if (value == 1) {
        attrs.push('checked="checked"');
        attrs.push('');
        attrs.push('disabled="disabled"');
    } else if (value == 2) {
        attrs.push('checked="checked"');
        attrs.push('checked="checked"');
        attrs.push('');
    } else if (value == 3) {
        attrs.push('checked="checked"');
        attrs.push('checked="checked"');
        attrs.push('checked="checked"');
    } else {
        attrs.push('');
        attrs.push('disabled="disabled"');
        attrs.push('disabled="disabled"');
    }


    if (qxg) {
        actions.push('<span data-id="' + row[field] + '" data-type="' + type + '" type="checkbox" class="ck1" ' + attrs[0] + ' >&nbsp;&nbsp;请修改&nbsp;&nbsp;</span>');
    } else {
        actions.push('<input data-id="' + row[field] + '" data-type="' + type + '" type="checkbox" class="ck1" ' + attrs[0] +'>&nbsp;&nbsp;请修改&nbsp;&nbsp;</input>');
    }
    if (yxg) {
        actions.push('<span data-id="' + row[field] + '" data-type="' + type + '" type="checkbox" class="ck2" ' + attrs[1] + ' >&nbsp;&nbsp;已修改&nbsp;&nbsp;</span>');
    } else {
        actions.push('<input data-id="' + row[field] + '" data-type="' + type + '" type="checkbox" class="ck2" ' + attrs[1] + '>&nbsp;&nbsp;已修改&nbsp;&nbsp;</input>');
    }

    if (ysh) {
        actions.push('<span data-id="' + row[field] + '" data-type="' + type + '" type="checkbox" class="ck3"' + attrs[2] + ' >&nbsp;&nbsp;审核通过&nbsp;&nbsp;</span>');
    } else {
        actions.push('<input data-id="' + row[field] + '" data-type="' + type + '" type="checkbox" class="ck3" ' + attrs[2] + '>&nbsp;&nbsp;审核通过&nbsp;&nbsp;</input>');
    }
    return actions.join('');
}

