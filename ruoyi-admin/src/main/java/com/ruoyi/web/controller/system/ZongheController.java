package com.ruoyi.web.controller.system;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.SysCategory;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.domain.Zhengqu;
import com.ruoyi.system.domain.Zonghe;
import com.ruoyi.system.service.ISysCategoryService;
import com.ruoyi.system.service.IZongheService;
import com.ruoyi.system.util.MyUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 综合 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
@Controller
@RequestMapping("/system/zonghe")
public class ZongheController extends BaseController
{
	private static final Logger log = LoggerFactory.getLogger(SysProfileController.class);
    private String prefix = "system/zonghe";
	@Autowired
	private MyWebScoketController websc;
	@Autowired
	private ISysCategoryService categoryService;
	@Autowired
	private IZongheService zongheService;
	
	@RequiresPermissions("system:zonghe:view")
	@GetMapping()
	public String zonghe()
	{
	    return prefix + "/zonghe";
	}
	
	/**
	 * 查询综合列表
	 */
	@RequiresPermissions("system:zonghe:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Zonghe zonghe)
	{
		startPage();
		zonghe.setZhRoleName(ShiroUtils.getSysUser().getUserName());
		zongheService.updateZongheRoleName(zonghe);
		zonghe.setZhTitle(MyUtil.getSql(zonghe.getZhTitle(),"zh_title"));
		zonghe.setZhDetails(MyUtil.getSql(zonghe.getZhDetails(),"zh_details"));
        List<Zonghe> list = zongheService.selectZongheList(zonghe);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出综合列表
	 */
	@RequiresPermissions("system:zonghe:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Zonghe zonghe)
    {
		zonghe.setZhTitle(MyUtil.getSql(zonghe.getZhTitle(),"zh_title"));
		zonghe.setZhDetails(MyUtil.getSql(zonghe.getZhDetails(),"zh_details"));

		List<Zonghe> list = zongheService.selectZongheList(zonghe);
        ExcelUtil<Zonghe> util = new ExcelUtil<Zonghe>(Zonghe.class);
        return util.exportExcel(list, "综合信息");
    }
	
	/**
	 * 新增综合
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存综合
	 */
	@RequiresPermissions("system:zonghe:add")
	@Log(title = "综合", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Zonghe zonghe)
	{
		zonghe.setCreateBy(ShiroUtils.getLoginName());
		return toAjax(zongheService.insertZonghe(zonghe));
	}

	/**
	 * 修改综合
	 */
	@GetMapping("/edit/{zhId}")
	public String edit(@PathVariable("zhId") Integer zhId, ModelMap mmap)
	{
		Zonghe zonghe = zongheService.selectZongheById(zhId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(zonghe.getZhChangedate()!=null) {
			String changedateString = formatter.format(zonghe.getZhChangedate());
			zonghe.setZhNewChangedate(changedateString);
		}
		mmap.put("zonghe", zonghe);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存综合
	 */
	@RequiresPermissions("system:zonghe:edit")
	@Log(title = "综合", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Zonghe zonghe)
	{
		zonghe.setUpdateBy(ShiroUtils.getLoginName());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition pos = new ParsePosition(0);
		Date currentTime_2  = formatter.parse(zonghe.getZhNewChangedate(),pos);
		zonghe.setZhChangedate(currentTime_2);
		return toAjax(zongheService.updateZonghe(zonghe));
	}
	
	/**
	 * 删除综合
	 */
	@RequiresPermissions("system:zonghe:remove")
	@Log(title = "综合", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(zongheService.deleteZongheByIds(ids));
	}

	/**
	 * 预览区划地名
	 */
	@GetMapping("/info/{zhId}")
	public String info(@PathVariable("zhId") Integer zhId, ModelMap mmap)
	{
		Zonghe zonghe = zongheService.selectZongheById(zhId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(zonghe.getCreateTime());
		if(zonghe.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(zonghe.getUpdateTime());
			zonghe.setNewupdateTime(updatedateString);
		}
		if(zonghe.getZhChangedate()!=null) {
			String changedateString = formatter.format(zonghe.getZhChangedate());
			zonghe.setZhNewChangedate(changedateString);
		}
		zonghe.setNewcreateTime(createdateString);

		mmap.put("zonghe", zonghe);
		return prefix + "/info";
	}

	/**
	 * 预览区划地名
	 */
	@GetMapping("/infoFront/{zhId}")
	public String infoFront(@PathVariable("zhId") Integer zhId, ModelMap mmap, SysCategory category, String title)
	{


		SysCategory categoryNew = new SysCategory();

		if (category == null) {
			category = new SysCategory();
		}

		//所有的节点
		SysCategory c = new SysCategory();
		c.setParentId(100L);
		List<SysCategory> categoryList = categoryService.selectDeptList(c);

		//子目录
		List<SysCategory> childs = new ArrayList<>();

		if (category.getLevel() == null || category.getLevel() == 1) {
			if (category.getDeptId() == null) {
				categoryNew.setParentId(categoryList.get(0).getDeptId());
			} else {
				for (SysCategory n : categoryList) {
					if (n.getDeptId() == category.getDeptId()) {
						n.setCurr(true);
						categoryNew.setParentId(n.getDeptId());
					}
				}
			}
			childs = categoryService.selectDeptList(categoryNew);

		} else if (category.getLevel() == 2) {
			category = categoryService.selectDeptById(category.getDeptId());
			category.setCurr(true);
			categoryNew.setParentId(category.getParentId());
			//选中父级
			for (SysCategory n : categoryList) {
				if (n.getDeptId() == category.getParentId()) {
					n.setCurr(true);
				}
			}
			childs = categoryService.selectDeptList(categoryNew);
			//选中子级
			for (SysCategory c1 : childs) {
				if (c1.getDeptId() == category.getDeptId()) {
					System.out.println(c1.getDeptId() + "=="+ category.getDeptId());
					c1.setCurr(true);
				}
			}
		}
		mmap.put("nodes", categoryList);
		mmap.put("title", title);
		mmap.put("curr", categoryNew);
		mmap.put("curr_child", category);
		mmap.put("childs", childs);

		Zonghe zonghe = zongheService.selectZongheById(zhId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String createdateString = formatter.format(zonghe.getCreateTime());
		if(zonghe.getUpdateTime()!=null)
		{
			String updatedateString = formatter.format(zonghe.getUpdateTime());
			zonghe.setNewupdateTime(updatedateString);
		}
		if(zonghe.getZhChangedate()!=null) {
			String changedateString = formatter.format(zonghe.getZhChangedate());
			zonghe.setZhNewChangedate(changedateString);
		}
		zonghe.setNewcreateTime(createdateString);

		mmap.put("zonghe", zonghe);
		return prefix + "/infoFront";
	}

	/**
	 * 图面化
	 */
	@GetMapping("/avatar/{zhId}")
	public String avatar(@PathVariable("zhId") Integer zhId, ModelMap mmap) {
		Zonghe zonghe = zongheService.selectZongheById(zhId);
		mmap.put("zonghe", zonghe);
		return prefix + "/avatar";
	}
	/**
	 * 保存图像
	 */
	@Log(title = "图面化信息", businessType = BusinessType.UPDATE)
	@PostMapping("/updateAvatar")
	@ResponseBody
	public AjaxResult updateAvatar(MultipartFile file, Zonghe zh) {
		try {
			zh.setEditorBy(ShiroUtils.getLoginName());
			return toAjax(zongheService.updateZongheImg(zh));

           /* if (file != null && !file.isEmpty()) {
//				if (zongheService.updateZonghe(zonghe) > 0)
//				{
//					setSysUser(zongheService.selectZongheById(zonghe.getUserId()));
//					return success();
//				}
            } else {
                return error();
            }*/
		} catch (Exception e) {
			log.error("修改头像失败！", e);
			return error(e.getMessage());
		}
	}

	/**
	 * 审批交通
	 */
	@GetMapping("/shenpi/{zhId}")
	public String shenpi(@PathVariable("zhId") Integer zhId, ModelMap mmap) {
		Zonghe zonghe = zongheService.selectZongheById(zhId);
		mmap.put("zonghe", zonghe);
		return prefix + "/shenpi";
	}

	/**
	 * 审批保存交通
	 */
	@Log(title = "交通", businessType = BusinessType.UPDATE)
	@PostMapping("/shenpiSave")
	@ResponseBody
	public AjaxResult shenpiSave(Zonghe zonghe) throws IOException {
		SysNotice notice = new SysNotice();
		notice.setNoticeType(zonghe.getZhStatus());
		Zonghe jt = zongheService.selectZongheById(zonghe.getZhId());
		notice.setNoticeTitle("标题为" + "【" + jt.getZhTitle() + "】" + "的地图现势资料信息"+ "：审批通过" );
		websc.sendInfo(JSONObject.toJSONString(notice));
		return toAjax(zongheService.updateZongheShenpi(zonghe));
	}

	/**
	 * 预览审批交通
	 */
	@GetMapping("/shenpiinfo/{zhId}")
	public String shenpiinfo(@PathVariable("zhId") Integer zhId, ModelMap mmap) {
		Zonghe zonghe = zongheService.selectZongheById(zhId);
		mmap.put("zonghe", zonghe);
		return prefix + "/shenpiinfo";
	}

	/**
	 * 预览图面化交通
	 */
	@GetMapping("/avatarinfo/{zhId}")
	public String avatarinfo(@PathVariable("zhId") Integer zhId, ModelMap mmap) {
		Zonghe zonghe = zongheService.selectZongheById(zhId);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String editordateString = formatter.format(zonghe.getEditorTime());
		zonghe.setNeweditorTime(editordateString);
		mmap.put("zonghe", zonghe);
		return prefix + "/avatarinfo";
	}
}
