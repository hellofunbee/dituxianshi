package com.ruoyi.system.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component("GlobalProperties")
public class GlobalProperties {

    @Autowired
    private Environment environment;

    @Value("${ruoyi.profile}")
    private String serverPort;

    public Environment getEnv() {
        return environment;
    }
}