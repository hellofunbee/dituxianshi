package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Jiaotong;
import java.util.List;

/**
 * 交通 数据层
 *
 * @author ruoyi
 * @date 2019-03-21
 */
public interface JiaotongMapper
{
	/**
	 * 查询交通信息
	 *
	 * @param jtId 交通ID
	 * @return 交通信息
	 */
	public Jiaotong selectJiaotongById(Integer jtId);

	/**
	 * 查询交通信息
	 *
	 * @param jtId 交通ID
	 * @return 交通信息
	 */
	public Jiaotong selectJiaotongByIdInfo(Integer jtId);

	/**
	 * 查询交通列表
	 *
	 * @param jiaotong 交通信息
	 * @return 交通集合
	 */
	public List<Jiaotong> selectJiaotongList(Jiaotong jiaotong);

	/**
	 * 新增交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	public int insertJiaotong(Jiaotong jiaotong);

	/**
	 * 修改交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	public int updateJiaotong(Jiaotong jiaotong);

	/**
	 * 图面化交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	public int updateJiaotongImg(Jiaotong jiaotong);


	/**
	 * 图面化交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	public int updateJiaotongRoleName(Jiaotong jiaotong);

	/**
	 * 审批交通
	 *
	 * @param jiaotong 交通信息
	 * @return 结果
	 */
	public int updateJiaotongShenpi(Jiaotong jiaotong);

	/**
	 * 删除交通
	 *
	 * @param jtId 交通ID
	 * @return 结果
	 */
	public int deleteJiaotongById(Integer jtId);

	/**
	 * 批量删除交通
	 *
	 * @param jtIds 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteJiaotongByIds(String[] jtIds);

}