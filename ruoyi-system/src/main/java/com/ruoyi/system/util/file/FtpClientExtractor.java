package com.ruoyi.system.util.file;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;

public interface FtpClientExtractor {
	public void doInFtp(FTPClient client) throws IOException;
}
