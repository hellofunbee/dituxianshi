package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Zonghe;
import java.util.List;	

/**
 * 综合 数据层
 * 
 * @author ruoyi
 * @date 2019-04-10
 */
public interface ZongheMapper 
{
	/**
     * 查询综合信息
     * 
     * @param zhId 综合ID
     * @return 综合信息
     */
	public Zonghe selectZongheById(Integer zhId);
	
	/**
     * 查询综合列表
     * 
     * @param zonghe 综合信息
     * @return 综合集合
     */
	public List<Zonghe> selectZongheList(Zonghe zonghe);
	
	/**
     * 新增综合
     * 
     * @param zonghe 综合信息
     * @return 结果
     */
	public int insertZonghe(Zonghe zonghe);
	
	/**
     * 修改综合
     * 
     * @param zonghe 综合信息
     * @return 结果
     */
	public int updateZonghe(Zonghe zonghe);

	/**
	 * 修改综合
	 *
	 * @param zonghe 综合信息
	 * @return 结果
	 */
	public int updateZongheRoleName(Zonghe zonghe);
	
	/**
     * 删除综合
     * 
     * @param zhId 综合ID
     * @return 结果
     */
	public int deleteZongheById(Integer zhId);
	
	/**
     * 批量删除综合
     * 
     * @param zhIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteZongheByIds(String[] zhIds);

	/**
	 * 图面化交通
	 *
	 * @param zonghe 交通信息
	 * @return 结果
	 */
	public int updateZongheImg(Zonghe zonghe);

	/**
	 * 审批交通
	 *
	 * @param zonghe 交通信息
	 * @return 结果
	 */
	public int updateZongheShenpi(Zonghe zonghe);
}