package com.ruoyi.web.controller.system;

import com.ruoyi.common.page.TableDataInfo;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.system.util.MyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/front")
public class CmsFront extends BaseController {
    private String prefix = "system/front";

    @Autowired
    private IJiaotongService jiaotongService;
    @Autowired
    private ILvyouService lvyouService;
    @Autowired
    private IShuixiService shuixiService;
    @Autowired
    private IZongheService zongheService;
    @Autowired
    private IZhengquService zhengquService;
    @Autowired
    private ISysCategoryService categoryService;

    @GetMapping()
    public String front(ModelMap modelMap, SysCategory category, String title) {

        SysCategory categoryNew = new SysCategory();

        if (category == null) {
            category = new SysCategory();
        }

        //所有的节点
        SysCategory c = new SysCategory();
        c.setParentId(100L);
        List<SysCategory> categoryList = categoryService.selectDeptList(c);

        //子目录
        List<SysCategory> childs = new ArrayList<>();

        if (category.getLevel() == null || category.getLevel() == 1) {
            if (category.getDeptId() == null) {
                categoryNew.setParentId(categoryList.get(0).getDeptId());
            } else {
                for (SysCategory n : categoryList) {
                    if (n.getDeptId() == category.getDeptId()) {
                        n.setCurr(true);
                        categoryNew.setParentId(n.getDeptId());
                    }
                }
            }
            childs = categoryService.selectDeptList(categoryNew);

        } else if (category.getLevel() == 2) {
            category = categoryService.selectDeptById(category.getDeptId());
            category.setCurr(true);
            categoryNew.setParentId(category.getParentId());
            //选中父级
            for (SysCategory n : categoryList) {
                if (n.getDeptId() == category.getParentId()) {
                    n.setCurr(true);
                }
            }
            childs = categoryService.selectDeptList(categoryNew);
            //选中子级
            for (SysCategory c1 : childs) {
                if (c1.getDeptId() == category.getDeptId()) {
                    System.out.println(c1.getDeptId() + "=="+ category.getDeptId());
                    c1.setCurr(true);
                }
            }
        }
        modelMap.put("nodes", categoryList);
        modelMap.put("title", title);
        modelMap.put("curr", categoryNew);
        modelMap.put("curr_child", category);
        modelMap.put("childs", childs);


        if (category.getDeptId() != null) {

            Long deptId = category.getDeptId();
            SysCategory sysCategory = categoryService.selectDeptById(deptId);
            String ancestors = sysCategory.getAncestors();


            if (deptId == 4L) {
                //区域地名
                return prefix + "/zqFront";
            } else if (deptId == 6L) {
                //水系
                return prefix + "/sxFront";
            } else if (deptId == 7L) {
                //综合
                return prefix + "/zhFront";
            } else if (deptId == 102L) {
                //旅游
                return prefix + "/lvFront";
            } else if (deptId == 101L) {
                //交通
                return prefix + "/jtFront";
            } else if (ancestors.contains("4")) {
                //区域地名
                return prefix + "/zqFront";
            } else if (ancestors.contains("6")) {
                //水系
                return prefix + "/sxFront";
            } else if (ancestors.contains("7")) {
                //综合
                return prefix + "/zhFront";
            } else if (ancestors.contains("102")) {
                //旅游
                return prefix + "/lvFront";
            } else if (ancestors.contains("101")) {
                //交通
                return prefix + "/jtFront";
            }


        }
        return prefix + "/jtFront";
    }

    @GetMapping("/search")
    public String search(ModelMap modelMap, SearchDomain search) {
        String keyWords = search.getKeyWords();
        Long deptId = search.getDeptId();

        if (deptId == null ||"".equals(deptId)){
            deptId = 101L;
        }

        modelMap.put("keyWords", search.getKeyWords());
        modelMap.put("deptId", deptId);


        SysCategory sysCategory = categoryService.selectDeptById(deptId);
        String ancestors = sysCategory.getAncestors();
        if (deptId == 4L) {
            //区域地名
            return prefix + "/zqsearch";
        } else if (deptId == 6L) {
            //水系
            return prefix + "/sxsearch";
        } else if (deptId == 7L) {
            //综合
            return prefix + "/zhsearch";
        } else if (deptId == 102L) {
            //旅游
            return prefix + "/lvsearch";
        } else if (deptId == 101L) {
            //交通
            return prefix + "/jtsearch";
        } else if (ancestors.contains("4")) {
            //区域地名
            return prefix + "/zqsearch";
        } else if (ancestors.contains("6")) {
            //水系
            return prefix + "/sxsearch";
        } else if (ancestors.contains("7")) {
            //综合
            return prefix + "/zhsearch";
        } else if (ancestors.contains("102")) {
            //旅游
            return prefix + "/lvsearch";
        } else if (ancestors.contains("101")) {
            //交通
            return prefix + "/jtsearch";
        }
        return "jtFront";
    }

    /**
     * 查询数据列表
     */
    @PostMapping("/list")
    @ResponseBody
    public Object getInfo(SearchDomain search) {
        SysCategory node = null;
        TableDataInfo tb = null;
        Long deptId = search.getDeptId();

        if (deptId != null) {
            SysCategory sysCategory = categoryService.selectDeptById(deptId);
            String ancestors = ",";
            if (sysCategory != null) {
                ancestors += sysCategory.getDeptId() + "," + sysCategory.getAncestors() + ",";
            }

            String calss = "," + deptId + ",";
            startPage();

            if (ancestors.indexOf(",101,") > -1) {
                Jiaotong sj = new Jiaotong();
                sj.getParams().put("classId", calss);
                sj.setJtTitle(MyUtil.getSql(search.getKeyWords(),"jt_title"));

                List<Jiaotong> jiaotongList = jiaotongService.selectJiaotongList(sj);
                tb = getDataTable(jiaotongList);

            } else if (ancestors.indexOf(",102,") > -1) {
                Lvyou ly = new Lvyou();
                ly.getParams().put("classId", calss);
                ly.setLvTitle(MyUtil.getSql(search.getKeyWords(),"lv_title"));

                List<Lvyou> lvyouList = lvyouService.selectLvyouList(ly);
                tb = getDataTable(lvyouList);
            } else if (ancestors.indexOf(",4,") > -1) {
                Zhengqu zq = new Zhengqu();
                zq.getParams().put("classId", calss);
                zq.setZqTitle(MyUtil.getSql(search.getKeyWords(),"zq_title"));

                List<Zhengqu> zhengquList = zhengquService.selectZhengquList(zq);
                tb = getDataTable(zhengquList);
            } else if (ancestors.indexOf(",6,") > -1) {
                Shuixi sx = new Shuixi();
                sx.getParams().put("classId", calss);
                sx.setSxTitle(MyUtil.getSql(search.getKeyWords(),"sx_title"));

                List<Shuixi> shuixiList = shuixiService.selectShuixiList(sx);
                tb = getDataTable(shuixiList);
            } else if (ancestors.indexOf(",7,") > -1) {
                Zonghe zh = new Zonghe();
                zh.getParams().put("classId", calss);
                zh.setZhTitle(MyUtil.getSql(search.getKeyWords(),"zh_title"));

                zh.setZhTitle(search.getKeyWords());
                List<Zonghe> zongheList = zongheService.selectZongheList(zh);
                tb = getDataTable(zongheList);
            } else {
                Jiaotong sj = new Jiaotong();
                sj.getParams().put("classId", calss);
                sj.setJtTitle(MyUtil.getSql(search.getKeyWords(),"jt_title"));

                List<Jiaotong> jiaotongList = jiaotongService.selectJiaotongList(sj);
                tb = getDataTable(jiaotongList);
            }


        } else {
            Jiaotong sj = new Jiaotong();
            sj.setJtTitle(search.getKeyWords());
            List<Jiaotong> jiaotongList = jiaotongService.selectJiaotongList(sj);
            tb = getDataTable(jiaotongList);
        }


        return tb;
    }

    /*设置缩略图
    private List<CmsInfoDetail> setTinyPic(List<CmsInfoDetail> infos) {
        for (CmsInfoDetail info : infos) {

            try {
                String s = info.getfFile();
                File f = new File(Global.getPreFixPath() + File.separator + s);
                Document doc = Jsoup.parse(f, "utf-8");


                //取img
                Elements imgEles = doc.getElementsByTag("img");
                for (int i = 0; i < imgEles.size(); i++) {
                    Element ele = imgEles.get(i);
                    String url = ele.attr("src");

                    ImageIcon imageIcon = new ImageIcon(f.getParent() + File.separator + url);
                    int iconWidth = imageIcon.getIconWidth();
                    int iconHeight = imageIcon.getIconHeight();
                    if (iconWidth > 200 && iconHeight > 200) {
                        info.setFrontPic(new File(info.getfFile()).getParent() + File.separator + url);
                        break;
                    }

                }


            } catch (Exception e) {

            }
        }
        return infos;
    }
*/

}
